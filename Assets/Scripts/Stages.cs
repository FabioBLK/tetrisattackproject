﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stages {

    private JSONLevelGroup m_stages;
    private JSONProgress m_progress;

    public JSONLevelGroup StageGroup { get { return m_stages; } }
    public JSONProgress Progress { get { return m_progress; } }

    public Stages() {
        TextAsset levelsFile = Resources.Load<TextAsset>("levels");
        m_stages = JsonUtility.FromJson<JSONLevelGroup>(levelsFile.text);

        if (string.IsNullOrEmpty(PlayerPrefs.GetString("progress"))) {
            CreateProgress();
        }
        else {
            LoadProgress();
        }
    }

    public void UpdateProgress(int p_stageId, bool p_complete, StarsCount p_stars) {
        int stars = 1;
        if (p_stars == StarsCount.Two) {
            stars = 2;
        }
        else if (p_stars == StarsCount.Three) {
            stars = 3;
        }

        if(m_progress.levels[p_stageId-1].stars < stars) {
            m_progress.levels[p_stageId - 1].complete = p_complete;
            m_progress.levels[p_stageId - 1].stars = stars;
            SaveToPlayerPrefs();
            LoadProgress();
        }
    }

    private void CreateProgress() {
        JSONProgress jsonProgress = new JSONProgress();
        for (int i = 0; i < m_stages.levelGroup.Count; i++) {
            LevelProgress level = new LevelProgress(m_stages.levelGroup[i].id, false);
            jsonProgress.levels.Add(level);
        }

        string progressString = JsonUtility.ToJson(jsonProgress);
        PlayerPrefs.SetString("progress", progressString);

        LoadProgress();
    }

    private void LoadProgress() {
        string progressString = PlayerPrefs.GetString("progress");

        m_progress = JsonUtility.FromJson<JSONProgress>(progressString);
    }

    private void SaveToPlayerPrefs() {
        string progressString = JsonUtility.ToJson(m_progress);
        PlayerPrefs.SetString("progress", progressString);
    }
}
