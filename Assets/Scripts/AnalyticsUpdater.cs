﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public static class AnalyticsUpdater {

    public static void RegisterLevelStart(LevelInfo p_level) {
        AnalyticsEvent.LevelStart(p_level.name, p_level.id);
    }

    public static void RegisterLevelComplete(LevelInfo p_level, StarsCount p_starCount) {
        Dictionary<string, object> stars = new Dictionary<string, object> {
            { "stars", p_starCount.ToString() }
        };

        AnalyticsEvent.LevelComplete(p_level.name, p_level.id, stars);
    }

    public static void RegisterLevelFailed(LevelInfo p_level, string p_reason) {
        Dictionary<string, object> reasons = new Dictionary<string, object> {
            { "reason", p_reason }
        };

        AnalyticsEvent.LevelFail(p_level.name, p_level.id, reasons);
    }

    public static void RegisterLevelQuit(LevelInfo p_level) {
        AnalyticsEvent.LevelQuit(p_level.name, p_level.id);
    }

    public static void RegisterScreenVisit(ScreenName p_screen) {
        AnalyticsEvent.ScreenVisit(p_screen);
    }
}
