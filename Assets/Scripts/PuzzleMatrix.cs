﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleMatrix {

    private PuzzleBox[][] m_puzzleMatrix;
    private int m_matchNumber;

    private Queue<List<PuzzleBox>> m_toDestroyQueue = new Queue<List<PuzzleBox>>();
    private Queue<List<PuzzleBox>> m_toDropQueue = new Queue<List<PuzzleBox>>();

    private RowGameIndexNull m_boxExistsInRow;
    private int m_rowsGameFinishNumber = 0;

    private GameModeType m_currentGameMode;

    public RowGameIndexNull BoxExistsInRow { set { m_boxExistsInRow = value; } }

    public int RowsGameFinishNumber {
        set {
            if (ReferenceEquals(m_puzzleMatrix, null))
            {
                m_rowsGameFinishNumber = 0;
            }
            else
            {
                m_rowsGameFinishNumber = m_puzzleMatrix.Length + value;
            }
        }
    }

    public PuzzleMatrix(int p_rows, int p_columns, int p_matchNumber, GameModeType p_gameMode)
    {
        m_matchNumber = p_matchNumber;
        m_puzzleMatrix = new PuzzleBox[p_rows][];
        for (int i = 0; i < m_puzzleMatrix.Length; i++)
        {
            m_puzzleMatrix[i] = new PuzzleBox[p_columns];
        }

        int count = 1;
        for(int i = 0; i < m_puzzleMatrix.Length; i++)
        {
            for (int j = 0; j < m_puzzleMatrix[i].Length; j++)
            {
                //Debug.Log("Creating " + count);
                m_puzzleMatrix[i][j] = null;
                count++;
            }
        }

        m_currentGameMode = p_gameMode;
    }

    #region Public Methods
    public void InsertRow(PuzzleBox[] p_row)
    {
        MoveRowsUp();
        int lastRow = m_puzzleMatrix.Length - 1;
        for (int i = 0; i < m_puzzleMatrix[lastRow].Length; i++)
        {
            m_puzzleMatrix[lastRow][i] = p_row[i];
            if (!ReferenceEquals(p_row[i], null))
            {
                m_puzzleMatrix[lastRow][i].SetPosition(lastRow, i);
            }
        }

        for (int i = 0; i < m_puzzleMatrix[lastRow].Length; i++)
        {
            if (!ReferenceEquals(m_puzzleMatrix[lastRow][i], null))
            {
                CheckForMatch(m_puzzleMatrix[lastRow][i].BoxPosition, MoveDirection.Up);
            }
        }
        m_rowsGameFinishNumber--;
    }

    public void MovePuzzleBox(BoxPosition p_box, MoveDirection p_dir)
    {
        if (!BoxPositionExists(p_box))
        {
            return;
        }
        //Debug.Log(m_puzzleMatrix[p_box.row][p_box.column].BlockType.ToString());

        if (p_box.column >= m_puzzleMatrix[p_box.row].Length - 1 && p_dir == MoveDirection.Right)
        {
            return;
        }
        else if (p_box.column <= 0 && p_dir == MoveDirection.Left)
        {
            return;
        }
        else if (p_box.row >= m_puzzleMatrix.Length - 1 && p_dir == MoveDirection.Down)
        {
            return;
        }

        //check if boxes are animating
        if (m_puzzleMatrix[p_box.row][p_box.column].BlockType == Blocks.Animating)
        {
            return;
        }

        if (p_dir == MoveDirection.Left || p_dir == MoveDirection.Right)
        {
            bool swap = !ReferenceEquals(m_puzzleMatrix[p_box.row][p_box.column + p_dir.DirectionValue()], null);
            if (swap)
            {
                if (m_puzzleMatrix[p_box.row][p_box.column + p_dir.DirectionValue()].BlockType == Blocks.Animating)
                {
                    return;
                }
            }

            //Moving Box Game Objects
            BoxPosition boxNewPos = new BoxPosition(p_box.row, p_box.column + p_dir.DirectionValue());
            m_puzzleMatrix[p_box.row][p_box.column].MoveBox(p_dir, boxNewPos, CheckForMatch);


            if (swap)
            {
                boxNewPos = new BoxPosition(p_box.row, p_box.column);
                m_puzzleMatrix[p_box.row][p_box.column + p_dir.DirectionValue()].MoveBox(p_dir.OpositeDirection(), boxNewPos, CheckForMatch);
            }

            //Moving Box at the Matrix
            PuzzleBox temp = m_puzzleMatrix[p_box.row][p_box.column];
            if (swap)
            {
                m_puzzleMatrix[p_box.row][p_box.column] = m_puzzleMatrix[p_box.row][p_box.column + p_dir.DirectionValue()];
                m_puzzleMatrix[p_box.row][p_box.column + p_dir.DirectionValue()] = temp;
            }
            else
            {
                m_puzzleMatrix[p_box.row][p_box.column] = null;
                m_puzzleMatrix[p_box.row][p_box.column + p_dir.DirectionValue()] = temp;
            }
        }
        else if (p_dir == MoveDirection.Up || p_dir == MoveDirection.Down)
        {
            bool swap = !ReferenceEquals(m_puzzleMatrix[p_box.row + p_dir.DirectionValue()][p_box.column], null);
            if (swap)
            {
                if (m_puzzleMatrix[p_box.row + p_dir.DirectionValue()][p_box.column].BlockType == Blocks.Animating)
                {
                    return;
                }
            }

            //Moving Box Game Objects
            BoxPosition boxNewPos = new BoxPosition(p_box.row + p_dir.DirectionValue(), p_box.column);
            m_puzzleMatrix[p_box.row][p_box.column].MoveBox(p_dir, boxNewPos, CheckForMatch);


            if (swap)
            {
                boxNewPos = new BoxPosition(p_box.row, p_box.column);
                m_puzzleMatrix[p_box.row + p_dir.DirectionValue()][p_box.column].MoveBox(p_dir.OpositeDirection(), boxNewPos, CheckForMatch);
            }

            //Moving Box at the Matrix
            PuzzleBox temp = m_puzzleMatrix[p_box.row][p_box.column];
            if (swap)
            {
                m_puzzleMatrix[p_box.row][p_box.column] = m_puzzleMatrix[p_box.row + p_dir.DirectionValue()][p_box.column];
                m_puzzleMatrix[p_box.row + p_dir.DirectionValue()][p_box.column] = temp;
            }
            else
            {
                m_puzzleMatrix[p_box.row][p_box.column] = null;
                m_puzzleMatrix[p_box.row + p_dir.DirectionValue()][p_box.column] = temp;
            }
        }
        GameController.Instance.ConfirmMoveOccured();
    }

    public void CheckForMatch(BoxPosition p_box, MoveDirection p_dir)
    {
        //Debug.Log(m_puzzleMatrix[p_box.row][p_box.column].BlockType.ToString() + " finished moving");

        PuzzleBox movedBox = m_puzzleMatrix[p_box.row][p_box.column];

        if (WillBlockFall(p_box))
        {
            DroppingBoxes(new List<PuzzleBox>() { movedBox });
            List<PuzzleBox> previousDropBoxes = GetDropBoxes(p_box, new List<PuzzleBox>());
            DroppingBoxes(previousDropBoxes);
            return;
        }

        Blocks blockType = movedBox.BlockType;
        List<PuzzleBox> horizontalMatches = new List<PuzzleBox>()
        {
            movedBox
        };

        if(m_currentGameMode == GameModeType.Row && m_boxExistsInRow != null)
        {
            CheckForRowsGameFinish();
        }

        if (p_dir == MoveDirection.Right)
        {
            horizontalMatches = CheckForward(p_box, blockType, horizontalMatches);
        }
        else if (p_dir == MoveDirection.Left)
        {
            horizontalMatches = CheckBackward(p_box, blockType, horizontalMatches);
        }
        else if (p_dir == MoveDirection.Down || p_dir == MoveDirection.Up)
        {
            horizontalMatches = CheckLeftRight(p_box, blockType, horizontalMatches);
        }

        List<PuzzleBox> verticalMatches = new List<PuzzleBox>()
        {
            movedBox
        };

        verticalMatches = CheckUpDown(p_box, blockType, verticalMatches);

        List<PuzzleBox> dropBoxes = GetDropBoxes(p_box, horizontalMatches);

        BoxPosition higherVertical = new BoxPosition(m_puzzleMatrix.Length, 0);
        for (int i = 0; i < verticalMatches.Count; i++)
        {
            if (!horizontalMatches.Contains(verticalMatches[i]))
            {
                horizontalMatches.Add(verticalMatches[i]);
            }
            if (verticalMatches[i].BoxPosition.row < higherVertical.row)
            {
                higherVertical = verticalMatches[i].BoxPosition;
            }
        }

        if (higherVertical.row < m_puzzleMatrix.Length && higherVertical.row > 0)
        {
            dropBoxes.Add(m_puzzleMatrix[higherVertical.row - 1][higherVertical.column]);
        }

        //DestroyMatched(horizontalMatches);
        //DroppingBoxes(dropBoxes);

        if(horizontalMatches.Count == 0 && dropBoxes.Count > 0)
        {
            DroppingBoxes(dropBoxes);
        }
        else if (horizontalMatches.Count > 0 && dropBoxes.Count > 0)
        {
            m_toDestroyQueue.Enqueue(horizontalMatches);
            m_toDropQueue.Enqueue(dropBoxes);
        }
        else if(horizontalMatches.Count > 0 && dropBoxes.Count == 0)
        {
            m_toDestroyQueue.Enqueue(horizontalMatches);
            m_toDropQueue.Enqueue(dropBoxes);
        }

        StartDestroyAnimation(horizontalMatches);
    }

    public void PrintRows()
    {
        for (int i = 0; i < m_puzzleMatrix.Length; i++)
        {
            string row = "Row " + i + " = ";
            for (int j = 0; j < m_puzzleMatrix[i].Length; j++)
            {
                if (m_puzzleMatrix[i][j] != null)
                    row += string.Format("{0} is at row {1} / column {2} | ", 
                        m_puzzleMatrix[i][j].BlockType.ToString(), 
                        m_puzzleMatrix[i][j].BoxPosition.row, 
                        m_puzzleMatrix[i][j].BoxPosition.column
                        );
            }
            Debug.Log(row);
        }
    }

    public bool BoxPositionExists(BoxPosition p_pos)
    {
        return !ReferenceEquals(m_puzzleMatrix[p_pos.row][p_pos.column], null);
    }
    #endregion

    #region Private Methods
    private List<PuzzleBox> CheckForward(BoxPosition p_box, Blocks p_boxType, List<PuzzleBox> p_matchedList)
    {
        if(p_boxType == Blocks.Animating)
        {
            return new List<PuzzleBox>();
        }
        //Check forward
        int columnLength = m_puzzleMatrix[p_box.row].Length;
        if ((p_box.column + m_matchNumber) <= columnLength)
        {
            int nextBoxIndex = p_box.column + 1;
            bool checking =
                (nextBoxIndex < columnLength) &&
                !ReferenceEquals(m_puzzleMatrix[p_box.row][nextBoxIndex], null) &&
                m_puzzleMatrix[p_box.row][nextBoxIndex].BlockType == p_boxType;

            while (checking)
            {
                p_matchedList.Add(m_puzzleMatrix[p_box.row][nextBoxIndex]);

                nextBoxIndex++;

                checking =
                (nextBoxIndex < columnLength) &&
                !ReferenceEquals(m_puzzleMatrix[p_box.row][nextBoxIndex], null) &&
                m_puzzleMatrix[p_box.row][nextBoxIndex].BlockType == p_boxType;
            }
        }

        if (p_matchedList.Count >= m_matchNumber)
        {
            return p_matchedList;
        }
        else return new List<PuzzleBox>();
    }

    private List<PuzzleBox> CheckBackward(BoxPosition p_box, Blocks p_boxType, List<PuzzleBox> p_matchedList)
    {
        if (p_boxType == Blocks.Animating)
        {
            return new List<PuzzleBox>();
        }
        //Check backward
        if ((p_box.column + 1) - m_matchNumber >= 0)
        {
            int nextBoxIndex = p_box.column - 1;
            bool checking =
                (nextBoxIndex >= 0) &&
                !ReferenceEquals(m_puzzleMatrix[p_box.row][nextBoxIndex], null) &&
                m_puzzleMatrix[p_box.row][nextBoxIndex].BlockType == p_boxType;

            while (checking)
            {
                p_matchedList.Add(m_puzzleMatrix[p_box.row][nextBoxIndex]);

                nextBoxIndex--;

                checking =
                (nextBoxIndex >= 0) &&
                !ReferenceEquals(m_puzzleMatrix[p_box.row][nextBoxIndex], null) &&
                m_puzzleMatrix[p_box.row][nextBoxIndex].BlockType == p_boxType;
            }
        }

        if (p_matchedList.Count >= m_matchNumber)
        {
            return p_matchedList;
        }
        else return new List<PuzzleBox>();
    }

    private List<PuzzleBox> CheckUpDown(BoxPosition p_box, Blocks p_boxType, List<PuzzleBox> p_matchedList)
    {
        if (p_boxType == Blocks.Animating)
        {
            return new List<PuzzleBox>();
        }
        //Checks up
        int nextBoxIndex = p_box.row - 1;
        bool checking =
            (nextBoxIndex >= 0) &&
            !ReferenceEquals(m_puzzleMatrix[nextBoxIndex][p_box.column], null) &&
            m_puzzleMatrix[nextBoxIndex][p_box.column].BlockType == p_boxType;

        while (checking)
        {
            if (!p_matchedList.Contains(m_puzzleMatrix[nextBoxIndex][p_box.column]))
            {
                p_matchedList.Add(m_puzzleMatrix[nextBoxIndex][p_box.column]);
            }

            nextBoxIndex--;

            checking =
            (nextBoxIndex >= 0) &&
            !ReferenceEquals(m_puzzleMatrix[nextBoxIndex][p_box.column], null) &&
            m_puzzleMatrix[nextBoxIndex][p_box.column].BlockType == p_boxType;
        }
        
        //Checks down
        int rowLength = m_puzzleMatrix.Length;

        nextBoxIndex = p_box.row + 1;
        checking =
            (nextBoxIndex < rowLength) &&
            !ReferenceEquals(m_puzzleMatrix[nextBoxIndex][p_box.column], null) &&
            m_puzzleMatrix[nextBoxIndex][p_box.column].BlockType == p_boxType;

        while (checking)
        {
            if (!p_matchedList.Contains(m_puzzleMatrix[nextBoxIndex][p_box.column]))
            {
                p_matchedList.Add(m_puzzleMatrix[nextBoxIndex][p_box.column]);
            }

            nextBoxIndex++;

            checking =
            (nextBoxIndex < rowLength) &&
            !ReferenceEquals(m_puzzleMatrix[nextBoxIndex][p_box.column], null) &&
            m_puzzleMatrix[nextBoxIndex][p_box.column].BlockType == p_boxType;
        }

        if (p_matchedList.Count >= m_matchNumber)
        {
            return p_matchedList;
        }
        else return new List<PuzzleBox>();
    }

    private List<PuzzleBox> CheckLeftRight(BoxPosition p_box, Blocks p_boxType, List<PuzzleBox> p_matchedList)
    {
        if (p_boxType == Blocks.Animating)
        {
            return new List<PuzzleBox>();
        }
        //Check backwards
        int nextBoxIndex = p_box.column - 1;
        bool checking =
            (nextBoxIndex >= 0) &&
            !ReferenceEquals(m_puzzleMatrix[p_box.row][nextBoxIndex], null) &&
            m_puzzleMatrix[p_box.row][nextBoxIndex].BlockType == p_boxType;

        while (checking)
        {
            if (!p_matchedList.Contains(m_puzzleMatrix[p_box.row][nextBoxIndex]))
            {
                p_matchedList.Add(m_puzzleMatrix[p_box.row][nextBoxIndex]);
            }

            nextBoxIndex--;

            checking =
            (nextBoxIndex >= 0) &&
            !ReferenceEquals(m_puzzleMatrix[p_box.row][nextBoxIndex], null) &&
            m_puzzleMatrix[p_box.row][nextBoxIndex].BlockType == p_boxType;
        }

        //Checks down
        int columnLength = m_puzzleMatrix[p_box.row].Length;

        nextBoxIndex = p_box.column + 1;
        checking =
            (nextBoxIndex < columnLength) &&
            !ReferenceEquals(m_puzzleMatrix[p_box.row][nextBoxIndex], null) &&
            m_puzzleMatrix[p_box.row][nextBoxIndex].BlockType == p_boxType;

        while (checking)
        {
            if (!p_matchedList.Contains(m_puzzleMatrix[p_box.row][nextBoxIndex]))
            {
                p_matchedList.Add(m_puzzleMatrix[p_box.row][nextBoxIndex]);
            }

            nextBoxIndex++;

            checking =
            (nextBoxIndex < columnLength) &&
            !ReferenceEquals(m_puzzleMatrix[p_box.row][nextBoxIndex], null) &&
            m_puzzleMatrix[p_box.row][nextBoxIndex].BlockType == p_boxType;
        }

        if (p_matchedList.Count >= m_matchNumber)
        {
            return p_matchedList;
        }
        else return new List<PuzzleBox>();
    }

    private void DestroyMatched()
    {
        List<PuzzleBox> matchedList = new List<PuzzleBox>();

        try
        {
            matchedList = m_toDestroyQueue.Dequeue();
        }
        catch
        {
            Debug.LogError("Error at DestroyMatched");
        }
        
        List<BoxPosition> toDropPos = new List<BoxPosition>();
        if (matchedList.Count >= m_matchNumber)
        {
            //Debug.LogWarning("destroying " + matchedList.Count + " blocks");
            for (int i = 0; i < matchedList.Count; i++)
            {
                if (!ReferenceEquals(matchedList[i], null))
                {
                    BoxPosition pos = matchedList[i].BoxPosition;
                    toDropPos.Add(new BoxPosition(pos.row, pos.column));
                    if (!ReferenceEquals(m_puzzleMatrix[pos.row][pos.column], null))
                    {
                        m_puzzleMatrix[pos.row][pos.column].DestroyPuzzleBox();
                        m_puzzleMatrix[pos.row][pos.column] = null;
                    }
                }
            }
        }

        Debug.Log("Destroy Finished");

        List<PuzzleBox> toDropList = new List<PuzzleBox>();
        for (int i = 0; i < toDropPos.Count; i++)
        {
            if (toDropPos[i].row > 0)
            {
                toDropList.Add(m_puzzleMatrix[toDropPos[i].row - 1][toDropPos[i].column]);
            }
        }
        DroppingBoxes(toDropList);
        DroppingBoxes(m_toDropQueue.Dequeue());
    }

    private void StartDestroyAnimation(List<PuzzleBox> p_matchedList)
    {
        if (p_matchedList.Count >= m_matchNumber)
        {
            GameController.Instance.MatchPause(p_matchedList.Count, p_matchedList[0].WorldPosition);
            //Debug.LogWarning("Destroy Animation for " + p_matchedList.Count + " blocks");
            for (int i = 0; i < p_matchedList.Count; i++)
            {
                bool first = true;
                if (!ReferenceEquals(p_matchedList[i], null))
                {
                    if (first)
                    {
                        p_matchedList[i].StartDestroyAnimation(DestroyMatched);
                        first = false;
                    }
                    else
                    {
                        p_matchedList[i].StartDestroyAnimation(DestroyMatched);
                    }
                }
            }
        }
    }

    private void DroppingBoxes(List<PuzzleBox> p_dropBoxes)
    {
        for(int i = 0; i < p_dropBoxes.Count; i++)
        {
            if (ReferenceEquals(p_dropBoxes[i], null))
            {
                continue;
            }

            if (p_dropBoxes[i].BoxPosition.row < m_puzzleMatrix.Length - 1)
            {
                int downRow = p_dropBoxes[i].BoxPosition.row + 1;
                int column = p_dropBoxes[i].BoxPosition.column;

                bool canFall = ReferenceEquals(m_puzzleMatrix[downRow][column], null) && downRow <= m_puzzleMatrix.Length - 1;
                while (canFall)
                {
                    downRow++;
                    if(downRow <= m_puzzleMatrix.Length - 1)
                    {
                        canFall = ReferenceEquals(m_puzzleMatrix[downRow][column], null);
                    }
                    else
                    {
                        canFall = false;
                    }
                }

                int dropPosition = downRow - 1;
                if (dropPosition > p_dropBoxes[i].BoxPosition.row)
                {
                    List<BoxPosition> upperBoxPositions = new List<BoxPosition>();
                    List<BoxPosition> newUpperBoxPositions = new List<BoxPosition>();
                    int upperBoxNewRow = dropPosition - 1;
                    for (int j = p_dropBoxes[i].BoxPosition.row - 1; j >= 0; j--)
                    {
                        if (!ReferenceEquals(m_puzzleMatrix[j][column], null))
                        {
                            upperBoxPositions.Add(new BoxPosition(j, column));
                            newUpperBoxPositions.Add(new BoxPosition(upperBoxNewRow, column));
                            upperBoxNewRow--;
                        }
                    }

                    BoxPosition originalPos = new BoxPosition(p_dropBoxes[i].BoxPosition.row, p_dropBoxes[i].BoxPosition.column);
                    p_dropBoxes[i].DropBox(new BoxPosition(dropPosition, column), CheckForMatch);
                    PuzzleBox tempBox = m_puzzleMatrix[originalPos.row][originalPos.column];
                    m_puzzleMatrix[originalPos.row][originalPos.column] = null;
                    m_puzzleMatrix[dropPosition][column] = tempBox;

                    for(int k = 0; k < upperBoxPositions.Count; k++)
                    {
                        m_puzzleMatrix[upperBoxPositions[k].row][column].DropBox(new BoxPosition(newUpperBoxPositions[k].row, column), CheckForMatch);
                        tempBox = m_puzzleMatrix[upperBoxPositions[k].row][column];
                        m_puzzleMatrix[upperBoxPositions[k].row][column] = null;
                        m_puzzleMatrix[newUpperBoxPositions[k].row][column] = tempBox;
                    }
                }
            }
        }
    }

    private bool WillBlockFall(BoxPosition p_pos)
    {
        int fallRow = p_pos.row + 1;
        if (fallRow <= m_puzzleMatrix.Length - 1)
        {
            if (ReferenceEquals(m_puzzleMatrix[fallRow][p_pos.column], null))
            {
                return true;
            }
        }

        return false;
    }

    private void MoveRowsUp()
    {
        bool endGame = false;
        for(int i = 0; i < m_puzzleMatrix.Length; i++)
        {
            if (i > 0)
            {
                for (int j = 0; j < m_puzzleMatrix[i - 1].Length; j++)
                {
                    m_puzzleMatrix[i - 1][j] = m_puzzleMatrix[i][j];
                }

                for (int j = 0; j < m_puzzleMatrix[i].Length; j++)
                {
                    if (!ReferenceEquals(m_puzzleMatrix[i][j], null))
                    {
                        m_puzzleMatrix[i][j].SetPosition(i - 1, j);
                    }
                }
            }
            else if (i == 0)
            {
                for (int j = 0; j < m_puzzleMatrix[i].Length; j++)
                {
                    if (!ReferenceEquals(m_puzzleMatrix[i][j], null))
                    {
                        //Call Game over
                        Debug.LogWarning("EndGame");
                        m_puzzleMatrix[i][j].StartEndGameAnimation();
                        endGame = true;
                    }
                }
            }
        }

        if (endGame) {
            GameController.Instance.CallEndGame("FillRows");
        }
    }

    private List<PuzzleBox> GetDropBoxes(BoxPosition p_box, List<PuzzleBox> horizontalMatches)
    {
        List<PuzzleBox> dropBoxes = new List<PuzzleBox>();
        for (int i = 0; i < horizontalMatches.Count; i++)
        {
            int upperRow = horizontalMatches[i].BoxPosition.row - 1;
            int column = horizontalMatches[i].BoxPosition.column;
            if (upperRow >= 0 && !ReferenceEquals(m_puzzleMatrix[upperRow][column], null))
            {
                PuzzleBox dropBox = m_puzzleMatrix[upperRow][column];
                if (!dropBoxes.Contains(dropBox))
                {
                    dropBoxes.Add(dropBox);
                }
            }
        }

        if (p_box.row > 0)
        {
            if (p_box.column < m_puzzleMatrix[p_box.row].Length - 1)
            {
                if (ReferenceEquals(m_puzzleMatrix[p_box.row][p_box.column + 1], null))
                {
                    dropBoxes.Add(m_puzzleMatrix[p_box.row - 1][p_box.column + 1]);
                }
            }

            if (p_box.column > 0)
            {
                if (ReferenceEquals(m_puzzleMatrix[p_box.row][p_box.column - 1], null))
                {
                    dropBoxes.Add(m_puzzleMatrix[p_box.row - 1][p_box.column - 1]);
                }
            }
        }

        return dropBoxes;
    }

    private void CheckForRowsGameFinish()
    {
        if (m_rowsGameFinishNumber < m_puzzleMatrix.Length && m_rowsGameFinishNumber >= 0)
        {
            int rowLength = m_puzzleMatrix[m_rowsGameFinishNumber].Length;
            int counter = 0;
            for (int i = 0; i < m_puzzleMatrix[m_rowsGameFinishNumber].Length; i++)
            {
                if (ReferenceEquals(m_puzzleMatrix[m_rowsGameFinishNumber][i], null))
                {
                    counter++;
                }
            }
            if (counter == rowLength)
            {
                m_boxExistsInRow();
            }
        }
        //Debug.Log("Boxes Moved");
    }
    #endregion
}
