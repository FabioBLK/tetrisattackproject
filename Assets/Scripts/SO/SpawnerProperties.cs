﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SpawnerProperties", menuName = "Spawner/SpawnerProperties", order = 1)]
public class SpawnerProperties : ScriptableObject {

    public float SideDistance;
    public float PerspectiveDistance;
    public GameObject BoxPrefab;
}
