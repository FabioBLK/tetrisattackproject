﻿using UnityEngine;

[CreateAssetMenu(fileName = "PuzzleBoxProperties", menuName = "Properties/PuzzleBoxProperties", order = 1)]
public class PuzzleBoxProperties : ScriptableObject {

    public Material BoxMaterial;
    public Material FadeMaterial;
    public Blocks BlockType;
    public GameObject MeshObject;
}
