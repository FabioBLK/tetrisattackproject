﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BaseProperties", menuName = "Base/BaseProperties", order = 1)]
public class BaseProperties : ScriptableObject {

    public GameModeType GameMode;
    public float RowTimeInterval;
    public float MinRowTimeInterval;
    public float MaxRowTimeInterval;
    [Range(1, 12)]
    public int RowsCount;
    [Range(1, 6)]
    public int ColumnsCount;
    public GameObject SpawnerPrefab;
    [Range(2, 4)]
    public int MatchNumber;
    public GameObject SwipeController;
    public GameObject CursorUI;
    public GameObject Cursor3D;
    public Vector2 CursorPosition;
    public float CursorSize;
    public Vector3 Cursor3DPosition;
    public Vector3 SpawnPosition;
    [Header("Sets the amount of time of pause for the amount of matches done.")]
    public float[] PauseTimer;
    [Header("Puzzle Boxes movement speed")]
    [Range(0, 2)]
    public float BlockMoveTime;
    [Range(0, 2)]
    public float BlockFallTime;
    [Header("Score Settings")]
    public int SinglePointScore;
    public int MultiplierScore;
    public int ComboScore;
    public float ScoreLevelThreshold;
    public float MaxScoreLevelThreshold;
    public int InitialScore;
    [Range(-40, 200)]
    public int ScoreMinBoost;
    [Range(-40, 200)]
    public int ScoreMaxBoost;
    [Range(-20, 100)]
    public float SpeedMinBoost;
    [Range(-20, 100)]
    public float SpeedMaxBoost;
    [Header("Row Mode Exclusive")]
    [Range(0, 100)]
    public int RowsToClear;
    public GameObject FinishGameIndicator;
    [Header("Combo Mode Exclusive")]
    [Range(0, 100)]
    public int CombosToClear;
    [Header("Level Mode Exclusive")]
    [Range(1, 100)]
    public int LevelsToClear;
    [Header("Level / Combo / Row Modes")]
    [Range(0, 1000)]
    public int MovesAvailable;
}
