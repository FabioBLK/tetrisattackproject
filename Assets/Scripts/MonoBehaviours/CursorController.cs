﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CursorController : MonoBehaviour {

    private Image m_cursor;
    private float m_minXPos = 0;
    private float m_minYPos = 0;
    private float m_cursorSize = 0;
    private int m_cursorColumn = 1;
    private float m_cursorRow = 1;
    private int m_totalCursorColumns = 1;
    private float m_totalCursorRows = 1;
    private int m_minCursorColumn = 1;
    private float m_minCursorRow = 1;
    private int m_maxCursorColumn = 1;
    private float m_maxCursorRow = 1;
    private bool m_moving = false;
    private float m_rowInterval = 1;
    private List<int> m_reverseRow = new List<int>();

    private float yOffset = 0;

	// Use this for initialization
	void Start ()
    {
        m_rowInterval = GameController.Instance.BaseProperties.RowTimeInterval;
        m_cursor = GetComponent<Image>();
        Vector2 startPos = GameController.Instance.BaseProperties.CursorPosition;
        m_cursorSize = GameController.Instance.BaseProperties.CursorSize * 0.99f;
        m_minXPos = startPos.x;
        m_totalCursorColumns = GameController.Instance.BaseProperties.ColumnsCount - 1;
        m_minCursorColumn = Mathf.RoundToInt(m_minXPos / m_cursorSize);
        m_maxCursorColumn = m_minCursorColumn + m_totalCursorColumns - 1;
        m_cursorColumn = m_minCursorColumn;

        m_minYPos = startPos.y;
        m_totalCursorRows = GameController.Instance.BaseProperties.RowsCount - 1;
        m_minCursorRow = Mathf.RoundToInt(m_minYPos / m_cursorSize);
        m_maxCursorRow = m_minCursorRow + m_totalCursorRows;
        m_cursorRow = m_minCursorRow;

        Vector2 newPos = new Vector2(m_cursorColumn * m_cursorSize, m_cursorRow * m_cursorSize);
        m_cursor.rectTransform.localPosition = newPos;

        for(int i = 0; i < m_totalCursorRows; i++)
        {
            m_reverseRow.Add(i);
        }
        m_reverseRow.Reverse();
    }
	
	// Update is called once per frame
	void Update ()
    {
        yOffset = Time.smoothDeltaTime *  m_cursorSize;
        yOffset = yOffset / m_rowInterval;
        //yOffset = yOffset * 0.99f;

        m_cursor.rectTransform.localPosition = new Vector2(m_cursor.rectTransform.localPosition.x, m_cursor.rectTransform.localPosition.y + yOffset);

        float currentRange = (m_cursor.rectTransform.localPosition.y / m_cursorSize) - m_minCursorRow;
        float xAxis = Input.GetAxisRaw("Horizontal");
        float yAxis = Input.GetAxisRaw("Vertical");

        if(m_moving && xAxis == 0 && yAxis == 0)
        {
            m_moving = false;
            return;
        }

        if (m_moving)
            return;

        if (xAxis == 1 || xAxis == -1 || yAxis == 1 || yAxis == -1)
        {
            m_moving = true;

            m_cursorColumn = m_cursorColumn + (int)xAxis;
            m_cursorColumn = Mathf.Clamp(m_cursorColumn, m_minCursorColumn, m_maxCursorColumn);

            if ((currentRange + (int)yAxis) > 0 && (currentRange + (int)yAxis) < m_totalCursorRows)
            {
                m_cursorRow = currentRange + (int)yAxis + m_minCursorRow;
                m_cursorRow = Mathf.Clamp(m_cursorRow, m_minCursorRow, m_maxCursorRow);

                Vector2 newPos = new Vector2(m_cursorColumn * m_cursorSize, m_cursorRow * m_cursorSize);
                m_cursor.rectTransform.localPosition = newPos;
            }            
            else
            {
                Vector2 newPos = new Vector2(m_cursorColumn * m_cursorSize, m_cursor.rectTransform.localPosition.y);
                m_cursor.rectTransform.localPosition = newPos;
            }
        }
        if (currentRange > m_totalCursorRows)
        {
            m_cursorRow = m_maxCursorRow - 1;
            Vector2 newPos = new Vector2(m_cursorColumn * m_cursorSize, m_cursorRow * m_cursorSize);
            m_cursor.rectTransform.localPosition = newPos;
        }

        if (Input.GetButtonDown("Fire1"))
        {
            int row = Mathf.FloorToInt(currentRange);
            int columnLeft = m_cursorColumn - m_minCursorColumn;
            int columnRight = m_cursorColumn - m_minCursorColumn + 1;
            int reverseRow = m_reverseRow[row];

            if(!GameController.Instance.PuzzleBoxExistsAt(new BoxPosition(reverseRow, columnLeft)) && GameController.Instance.PuzzleBoxExistsAt(new BoxPosition(reverseRow, columnRight)))
            {
                GameController.Instance.PuzzleBoxClicked(new BoxPosition(reverseRow, columnRight), MoveDirection.Left);
            }
            else
            {
                GameController.Instance.PuzzleBoxClicked(new BoxPosition(reverseRow, columnLeft), MoveDirection.Right);
            }

        }
    }
}
