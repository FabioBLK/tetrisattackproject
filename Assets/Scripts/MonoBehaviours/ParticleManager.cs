﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ParticleManager : MonoBehaviour {

    private static ParticleManager _instance;
    public static ParticleManager instance {
        get {
            if (_instance == null) {
                _instance = GameObject.FindObjectOfType<ParticleManager>();
                //DontDestroyOnLoad(_instance.gameObject);
            }
            return _instance;
        }
    }

    [Header("Adicione as Particulas aqui")]
    [SerializeField]
    ParticleSystem extraMatch;

    [SerializeField]
    ParticleSystem combo;

    [SerializeField]
    ParticleSystem startParticle;

    [SerializeField]
    ParticleSystem finishParticle;

    [SerializeField]
    ParticleSystem bump;

    [SerializeField]
    ParticleSystem star;

    [Header("Número máximo de particulas desse tipo simultaneamente")]
    [SerializeField]
    int maxExtraMatch = 3;

    [SerializeField]
    int maxCombo = 10;

    [SerializeField]
    int maxStartParticle = 3;

    [SerializeField]
    int maxFinishParticle = 10;

    [SerializeField]
    int maxBump = 5;

    [SerializeField]
    int maxStar = 5;

    void Start() {
        CFX_SpawnSystem.PreloadObject(extraMatch.gameObject, maxExtraMatch);
        CFX_SpawnSystem.PreloadObject(combo.gameObject, maxCombo);
        CFX_SpawnSystem.PreloadObject(startParticle.gameObject, maxStartParticle);
        CFX_SpawnSystem.PreloadObject(finishParticle.gameObject, maxFinishParticle);
        CFX_SpawnSystem.PreloadObject(bump.gameObject, maxBump);
        CFX_SpawnSystem.PreloadObject(star.gameObject, maxStar);
    }

    /// <summary>
    /// Executa a partícula de Combo uma vez
    /// </summary>
    /// <param name="particlePosition">Indique a posição em que essa partícula será executada</param>
    public void StartCombo(Vector3 particlePosition) {
        StartSingleParticle(combo.gameObject, particlePosition, true);
    }

    /// <summary>
    /// Executa a partícula de inicio
    /// </summary>
    /// <param name="particlePosition">Indique a posição em que essa partícula será executada</param>
    public void StartParticleStart(Vector3 particlePosition) {
        StartSingleParticle(startParticle.gameObject, particlePosition);
    }

    /// <summary>
    /// Executa a partícula de Finalizar
    /// </summary>
    /// <param name="particlePosition">Indique a posição em que essa partícula será executada</param>
    public void StartParticleFinish(Vector3 particlePosition) {
        StartSingleParticle(finishParticle.gameObject, particlePosition);
    }

    /// <summary>
    /// Executa a partícula de Bump
    /// </summary>
    /// <param name="particlePosition">Indique a posição em que essa partícula será executada</param>
    public void StartBump(Vector3 particlePosition) {
        StartSingleParticle(bump.gameObject, particlePosition, true);
    }

    /// <summary>
    /// Executa a partícula de Star
    /// </summary>
    /// <param name="particlePosition">Indique a posição em que essa partícula será executada</param>
    public void StartStar(Vector3 particlePosition) {
        StartSingleParticle(star.gameObject, particlePosition);
    }

    /// <summary>
    /// Executa a partícula de Extra Match
    /// </summary>
    /// <param name="particlePosition">Indique a posição em que essa partícula será executada</param>
    public void StartExtraMatch(Vector3 particlePosition) {
        StartSingleParticle(extraMatch.gameObject, particlePosition, true);
    }

    /// <summary>
    /// Executa a partícula de campo elétrico em loop.
    /// Atenção: Será necessário mandar o comando de Stop se precisar parar esse efeito
    /// </summary>
    /// <param name="particlePosition">Indique a posição em que essa partícula será executada</param>
    /// <param name="particleHolder">Obrigatório para particulas em loop, utilize esse objeto para indicar quem está invocando ou parando a particula.</param>
    public void StartElectricFieldLooped(Vector3 particlePosition, GameObject particleHolder) {
        StartLoopedParticle(extraMatch.gameObject, particlePosition, particleHolder);
    }

    /// <summary>
    /// Para a execução da particula em loop
    /// </summary>
    /// <param name="particleHolder">Forneca o mesmo objeto que iniciou a partícula</param>
    public void StopElectricFieldLooped(GameObject particleHolder) {
        StopLoopedParticle(particleHolder);
    }

    /// <summary>
    /// Para a execução da particula em loop
    /// </summary>
    /// <param name="particleHolder">Forneca o mesmo objeto que iniciou a partícula</param>
    public void StopSmallElectricFieldLooped(GameObject particleHolder) {
        StopLoopedParticle(particleHolder);
    }

    private void StartSingleParticle(GameObject particleObject, Vector3 particlePosition, bool p_offset = false) {
        GameObject particleInstance = CFX_SpawnSystem.GetNextObject(particleObject, true);
        Vector3 offset = Vector3.zero;
        if (p_offset) {
            offset = new Vector3(0, 0, -1.25f);
        }
        particleInstance.transform.position = particlePosition + offset;
        particleInstance.transform.Rotate(25, 0, 0);
    }

    private void StartLoopedParticle(GameObject particleObject, Vector3 particlePosition, GameObject particleHolder) {
        GameObject particleInstance = CFX_SpawnSystem.GetNextObject(particleObject, true);
        particleInstance.GetComponent<ParticleSystem>().loop = true;
        particleInstance.transform.position = particlePosition;
        particleInstance.transform.parent = particleHolder.transform;
    }

    private void StopLoopedParticle(GameObject particleHolder) {
        ParticleSystem[] ps = particleHolder.transform.GetComponentsInChildren<ParticleSystem>();
        for (int i = 0; i < ps.Length; i++) {
            ps[i].gameObject.transform.parent = this.transform;
            ps[i].gameObject.SetActive(false);
        }
    }

}
