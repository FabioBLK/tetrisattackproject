﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MusicManager : MonoBehaviour {

    private static MusicManager _instance;
    public static MusicManager instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = FindObjectOfType<MusicManager>();
                DontDestroyOnLoad(_instance);
            }
            return _instance;
        }
    }

    AudioSource myAudio;

    [Header("Volume - De 0 até 1")]
    [SerializeField]
    float maxVolume = 1;

    [SerializeField]
    float minVolume = 0;

    [Header("Músicas específicas")]
    [SerializeField]
    AudioClip openingSceneMusic;

    [SerializeField]
    AudioClip mainLevelMusic;

    [SerializeField]
    AudioClip mainLevel2Music;

    [SerializeField]
    AudioClip mainLevel3Music;

    public enum MusicList { Jukebox, OpeningScene, MainMusic, MainMusic2, MainMusic3 };
    MusicList musicSelect;

    [Header("Músicas sequenciais. Caso não queira específicar uma música específica para tocar")]
    [Tooltip("Faixas de músicas que podem ser tocadas em sequencia")]
    [SerializeField]
    List<AudioClip> jukeboxMusicList = new List<AudioClip>();

    [SerializeField]
    bool onMute = false;

    bool changingMusic = false;

    int currentMusicIndex = 0;
    int mainGameMusicIndex = 0;
    float currentVolume = 1;
    float thisMaxVolume = 0;

    public int CurrentMusicIndex { get { return currentMusicIndex; } }

    void Awake()
    {
        MusicManager[] findOther = FindObjectsOfType<MusicManager>();
        for (int i = 0; i < findOther.Length; i++) {

            if (findOther[i].gameObject != this.gameObject)
                Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this);
    }

	// Use this for initialization
	void Start () {
        myAudio = GetComponent<AudioSource>();
        thisMaxVolume = maxVolume;

        int musicEnabled = 0;
        musicEnabled = PlayerPrefs.GetInt("Music");

        bool enabled = (musicEnabled == 0);
        MuteMusic(!enabled);
    }
	
	// Update is called once per frame
	void Update () {

        if (changingMusic)
        {
            myAudio.volume = currentVolume;
            currentVolume -= 0.02f;
            if (currentVolume < minVolume)
            {
                SelectMusic();
            }
        }
        else
        {
            if (currentVolume < thisMaxVolume)
            {
                //print("volume up");
                currentVolume += 0.02f;
                myAudio.volume = currentVolume;
            }
        }


	}

    private void SelectMusic()
    {
        changingMusic = false;

        if (currentMusicIndex == 0)
        {
            PlayJukebox();
        }
        else if (currentMusicIndex == 1)
        {
            PlayOpening();
        }
        else if (currentMusicIndex == 2)
        {
            PlayMainMusic();
        }
        else if (currentMusicIndex == 3)
        {
            PlayMainMusic2();
        }

        else if (currentMusicIndex == 4)
        {
            PlayMainMusic3();
        }
    }

    /// <summary>
    /// Muda a música atual para uma nova música. Especificada pelo indice fornecido.
    /// </summary>
    /// <param name="musicIndex">Informe o índice da música desejada. 0 para utilizar a lista de músicas jukebox</param>
    public void ChangeMusic(int musicIndex)
    {
        changingMusic = true;
        currentMusicIndex = musicIndex;
    }

    /// <summary>
    /// Muda a música atual para uma nova música. Especificada pelo nome da música do Enum.
    /// </summary>
    /// <param name="musicFromList">Informe o nome da música desejada. 0 para utilizar a lista de músicas jukebox</param>
    public void ChangeMusic(MusicList musicFromList)
    {
        musicSelect = musicFromList;
        currentMusicIndex = (int)musicSelect;
        changingMusic = true;
    }

    void PlayOpening()
    {
        myAudio.Stop();
        myAudio.loop = false;
        myAudio.clip = openingSceneMusic;
        myAudio.Play();
    }

    void PlayMainMusic3()
    {
        myAudio.Stop();
        myAudio.loop = true;
        myAudio.clip = mainLevel3Music;
        myAudio.Play();
    }

    void PlayMainMusic()
    {
        myAudio.Stop();
        myAudio.loop = true;
        myAudio.clip = mainLevelMusic;
        myAudio.Play();
    }

    void PlayMainMusic2()
    {
        myAudio.Stop();
        myAudio.loop = true;
        myAudio.clip = mainLevel2Music;
        myAudio.Play();
    }

    void PlayJukebox()
    {
        myAudio.Stop();

        if (mainGameMusicIndex < jukeboxMusicList.Count - 1)
            mainGameMusicIndex++;
        else
            mainGameMusicIndex = 0;

        myAudio.clip = jukeboxMusicList[mainGameMusicIndex];
        
        myAudio.Play();
    }

    /// <summary>
    /// Alternar entre deixar o som mudo ou tocando
    /// </summary>
    /// <param name="value">true para mutar o audio / false para tocar o audio</param>
    public void MuteMusic(bool value, bool p_savePlayerPrefs = true)
    {
        if (value)
        {
            thisMaxVolume = 0;
            currentVolume = 0;
            myAudio.volume = thisMaxVolume;
        }
            
        else
        {
            thisMaxVolume = maxVolume;
            currentVolume = maxVolume;
            myAudio.volume = thisMaxVolume;
        }
        onMute = value;

        if (p_savePlayerPrefs) {
            if (onMute)
            {
                PlayerPrefs.SetInt("Music", 1);
            }
            else
            {
                PlayerPrefs.SetInt("Music", 0);
            }   
        }
    }

    /// <summary>
    /// Retorna o status do som. True para som mudo, False para som tocando
    /// </summary>
    /// <returns></returns>
    public bool IsMusicMute()
    {
        return onMute;
    }

    public void ForcePause(){
        myAudio.Pause();
    }

    public void ForceResume(){
        myAudio.UnPause();
    }

}
