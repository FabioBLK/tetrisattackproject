﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeController : MonoBehaviour {

    private Vector2 fingerDown;
    private Vector2 fingerUp;
    public bool detectSwipeOnlyAfterRelease = true;
    BoxMovement boxMovement = null;

    public float SWIPE_THRESHOLD = 20f;
    private bool m_gamePaused = false;

    private void Start() {
        GameController.Instance.ChangeUIPauseEvent += PauseGame;
    }

    // Update is called once per frame
    void Update()
    {
        if (m_gamePaused) {
            return;
        }

        if (Input.touches.Length > 1)
        {
            Debug.Log("Multi touch NOO");
            return;
        }
        foreach (Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                fingerUp = touch.position;
                fingerDown = touch.position;

                //Vector3 fwrd = cursorLeft.TransformDirection(Vector3.down);
                boxMovement = null;

                RaycastHit hitInfoLeft;
                Ray ray = Camera.main.ScreenPointToRay(fingerDown);
                //Debug.DrawRay(fingerDown, fwrd, Color.red, 1, true);
                if (Physics.Raycast(ray, out hitInfoLeft))
                {
                    boxMovement = hitInfoLeft.transform.GetComponent<BoxMovement>();
                    Debug.Log("Box position : " + boxMovement.PuzzleBox.BoxPosition);
                }
            }

            //Detects Swipe while finger is still moving
            if (touch.phase == TouchPhase.Moved)
            {
                if (!detectSwipeOnlyAfterRelease)
                {
                    fingerDown = touch.position;
                    checkSwipe();
                }
            }

            //Detects swipe after finger is released
            if (touch.phase == TouchPhase.Ended)
            {
                fingerDown = touch.position;
                checkSwipe();
            }
        }
    }

    void checkSwipe()
    {
        if (m_gamePaused) {
            return;
        }
        //Check if Vertical swipe
        if (verticalMove() > SWIPE_THRESHOLD && verticalMove() > horizontalValMove())
        {
            //Debug.Log("Vertical");
            if (fingerDown.y - fingerUp.y > 0)//up swipe
            {
                Up();
            }
            else if (fingerDown.y - fingerUp.y < 0)//Down swipe
            {
                Down();
            }
            fingerUp = fingerDown;
        }

        //Check if Horizontal swipe
        else if (horizontalValMove() > SWIPE_THRESHOLD && horizontalValMove() > verticalMove())
        {
            //Debug.Log("Horizontal");
            if (fingerDown.x - fingerUp.x > 0)//Right swipe
            {
                Right();
            }
            else if (fingerDown.x - fingerUp.x < 0)//Left swipe
            {
                Left();
            }
            fingerUp = fingerDown;
        }

        //No Movement at-all
        else
        {
            //Debug.Log("No Swipe!");
        }
    }

    float verticalMove()
    {
        return Mathf.Abs(fingerDown.y - fingerUp.y);
    }

    float horizontalValMove()
    {
        return Mathf.Abs(fingerDown.x - fingerUp.x);
    }

    private void PauseGame(bool p_value) {
        m_gamePaused = p_value;
    }

    //////////////////////////////////CALLBACK FUNCTIONS/////////////////////////////
    void Up()
    {
        Debug.Log("Swipe UP");
        if (boxMovement != null)
        {
            BoxPosition bPos = boxMovement.PuzzleBox.BoxPosition;
            GameController.Instance.PuzzleBoxClicked(bPos, MoveDirection.Up);
        }
    }

    void Down()
    {
        Debug.Log("Swipe Down");
        if (boxMovement != null)
        {
            BoxPosition bPos = boxMovement.PuzzleBox.BoxPosition;
            GameController.Instance.PuzzleBoxClicked(bPos, MoveDirection.Down);
        }
    }

    void Left()
    {
        Debug.Log("Swipe Left");
        if (boxMovement != null)
        {
            BoxPosition bPos = boxMovement.PuzzleBox.BoxPosition;
            GameController.Instance.PuzzleBoxClicked(bPos, MoveDirection.Left);
        }
    }

    void Right()
    {
        Debug.Log("Swipe Right");
        if (boxMovement != null)
        {
            BoxPosition bPos = boxMovement.PuzzleBox.BoxPosition;
            GameController.Instance.PuzzleBoxClicked(bPos, MoveDirection.Right);
        }
    }
}
