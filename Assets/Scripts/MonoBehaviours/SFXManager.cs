﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SFXManager : MonoBehaviour {

    private static SFXManager _instance;
    public static SFXManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<SFXManager>();
                DontDestroyOnLoad(_instance.gameObject);
            }
            return _instance;
        }
    }

    AudioSource myAudio;
	
	[SerializeField]
    AudioClip uiEnter;
	[SerializeField]
    AudioClip uiClick;
	[SerializeField]
    AudioClip stageStart;
	[SerializeField]
	AudioClip puppyMove;
	[SerializeField]
	AudioClip puppyFall;
    [SerializeField]
    AudioClip combo;
    [SerializeField]
    AudioClip matchFour;
    [SerializeField]
    AudioClip endGame;
    [SerializeField]
    AudioClip completeGame;
    [SerializeField]
    AudioClip cat;
    [SerializeField]
    AudioClip dog;
    [SerializeField]
    AudioClip panda;
    [SerializeField]
    AudioClip pig;
    [SerializeField]
    AudioClip lion;
    [SerializeField]
    AudioClip bird;

    [SerializeField]
    bool onMute = false;

    Dictionary<Blocks, AudioClip> blockSound = new Dictionary<Blocks, AudioClip>();

    void Awake()
    {
        SFXManager[] findOther = FindObjectsOfType<SFXManager>();
        for (int i = 0; i < findOther.Length; i++)
        {

            if (findOther[i].gameObject != this.gameObject)
                Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this);
    }


    void Start()
    {
        myAudio = GetComponent<AudioSource>();

        blockSound.Add(Blocks.Diamond, bird);
        blockSound.Add(Blocks.Fire, dog);
        blockSound.Add(Blocks.Gold, lion);
        blockSound.Add(Blocks.Heart, pig);
        blockSound.Add(Blocks.Water, cat);
        blockSound.Add(Blocks.Leaf, panda);
        blockSound.Add(Blocks.Animating, panda);

        int sfxEnabled = 0;
        sfxEnabled = PlayerPrefs.GetInt("SFX");

        bool enabled = (sfxEnabled == 0);
        MuteSFX(!enabled);
    }

    public void PlayUIEnter(float delay = 0)
    {
        if(delay > 0) {
            myAudio.clip = uiEnter;
            myAudio.PlayDelayed(delay);    
        }
        else {
            myAudio.PlayOneShot(uiEnter);
        }
    }

	public void PlayUIClick()
	{
		myAudio.PlayOneShot(uiClick);
	}

	public void PlayBoxStageStart()
	{
		myAudio.PlayOneShot(stageStart);
	}

	public void PlayPuppyMove()
	{
		myAudio.PlayOneShot(puppyMove);
	}

	public void PlayPuppyFall()
	{
        if (!myAudio.isPlaying) {
            myAudio.clip = puppyFall;
            myAudio.Play();
        }
	}

    public void PlayCombo()
    {
        myAudio.PlayOneShot(combo);
    }

    public void PlayMatchFour()
    {
        myAudio.PlayOneShot(matchFour);
    }

    public void PlayEndGame()
    {
        myAudio.PlayOneShot(endGame);

    }

    public void PlayCat()
    {
        myAudio.PlayOneShot(cat);
    }

    public void PlayCompleteGame()
    {
        myAudio.PlayOneShot(completeGame);
    }

    public void PlayDog()
    {
        myAudio.PlayOneShot(dog);
    }

    public void PlayPanda()
    {
        myAudio.PlayOneShot(panda);
    }

    public void PlayPig() {
        myAudio.PlayOneShot(pig);
    }

    public void PlayLion() {
        myAudio.PlayOneShot(lion);
    }

    public void PlayBird() {
        myAudio.PlayOneShot(bird);
    }

    public void PlayBlockSound(Blocks p_type) {
        myAudio.clip = blockSound[p_type];
        myAudio.Play();
    }

    /// <summary>
    /// Alternar entre deixar o som mudo ou tocando
    /// </summary>
    /// <param name="value">true para mutar o audio / false para tocar o audio</param>
    public void MuteSFX(bool value)
    {
        if (value)
        {
            myAudio.volume = 0;
        }

        else
        {
            myAudio.volume = 1;
        }
        onMute = value;

        if (onMute) {
            PlayerPrefs.SetInt("SFX", 1);
        }
        else {
            PlayerPrefs.SetInt("SFX", 0);
        }
    }

    /// <summary>
    /// Retorna o status do som. True para som mudo, False para som tocando
    /// </summary>
    /// <returns></returns>
    public bool IsSFXMute()
    {
        return onMute;
    }
}
