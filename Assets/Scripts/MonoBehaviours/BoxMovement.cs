﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BoxMovement : MonoBehaviour {

    private float m_fallTime = 0;
    private float m_moveTime = 0;
    private bool m_moving = false;
    private bool m_falling = false;
    private Vector3 m_moveDestination = Vector3.zero;
    private Vector3 m_startPosition = Vector3.zero;
    private PuzzleBox m_puzzleBox;
    private Action<BoxPosition, MoveDirection> m_callBack;
    private Action m_destroyCallBack;
    private MoveDirection m_moveDirection;
    private float m_fallTimer = 0;
    private float m_moveTimer = 0;
    private Vector3 velocity = Vector3.zero;
    private GameObject m_meshGameObject;
    private Animator m_animator;
    private Renderer m_renderer;

    public PuzzleBox PuzzleBox { get { return m_puzzleBox; } }

    private void Update () {
        if (m_moving)
        {
            if (m_falling)
            {
                m_fallTimer -= Time.deltaTime;
                float timer = m_fallTimer / m_fallTime;
                transform.localPosition = Vector3.SmoothDamp(transform.localPosition, m_moveDestination, ref velocity, timer);
                if (timer <= 0)
                {
                    m_falling = false;
                    SFXManager.instance.PlayPuppyFall();
                    ParticleManager.instance.StartBump(transform.position);
                    EndMovement();
                }
            }
            else
            {
                m_moveTimer += Time.deltaTime;
                float timer = m_moveTimer / m_moveTime;
                transform.localPosition = Vector3.MoveTowards(m_startPosition, m_moveDestination, timer);
                if (timer >= 1)
                {
                    EndMovement();
                }
            }
        }
	}

    private void EndMovement()
    {
        m_fallTimer = m_fallTime;
        m_moveTimer = 0;
        transform.localPosition = m_moveDestination;
        m_startPosition = transform.localPosition;
        m_moving = false;
        if (!ReferenceEquals(m_callBack, null))
        {
            m_puzzleBox.RevertBlockType();
            m_callBack(m_puzzleBox.BoxPosition, m_moveDirection);
        }
    }

    public void MoveBox(MoveDirection p_dir, Action<BoxPosition, MoveDirection> p_callBack)
    {
        if (m_moving)
        {
            EndMovement();
        }

        m_animator.SetTrigger("Jump Inplace");
        m_fallTimer = m_fallTime;
        m_moveTimer = 0;
        m_startPosition = transform.localPosition;
        m_callBack = p_callBack;
        m_moving = true;
        m_moveDirection = p_dir;

        if (p_dir == MoveDirection.Right)
        {
            m_moveDestination = new Vector3(transform.localPosition.x + 1, transform.localPosition.y, transform.localPosition.z);
        }
        else if(p_dir == MoveDirection.Left)
        {
            m_moveDestination = new Vector3(transform.localPosition.x - 1, transform.localPosition.y, transform.localPosition.z);
        }
        else if(p_dir == MoveDirection.Up)
        {
            m_moveDestination = new Vector3(transform.localPosition.x, transform.localPosition.y + 1, transform.localPosition.z);
        }
        else if(p_dir == MoveDirection.Down)
        {
            m_moveDestination = new Vector3(transform.localPosition.x, transform.localPosition.y - 1, transform.localPosition.z);
        }
    }

    public void DropBox(int p_fallPositions, Action<BoxPosition, MoveDirection> p_callBack)
    {
        if (m_moving)
        {
            transform.localPosition = m_moveDestination;
            m_moving = false;
        }

        m_animator.SetTrigger("Hit");
        m_falling = true;
        m_fallTimer = m_fallTime;
        m_moveTimer = 0;
        m_startPosition = transform.localPosition;
        m_callBack = p_callBack;
        m_moving = true;
        m_moveDirection = MoveDirection.Down;

        m_moveDestination = new Vector3(transform.localPosition.x, transform.localPosition.y - p_fallPositions, transform.localPosition.z);
        
    }

    public void SetPuzzleBox(PuzzleBox p_puzzleBox, float p_moveTime, float p_fallTime, GameObject p_meshGameObject)
    {
        m_puzzleBox = p_puzzleBox;
        m_moveTime = p_moveTime;
        m_fallTime = p_fallTime;
        m_meshGameObject = Instantiate(p_meshGameObject, transform.position + new Vector3(0, -0.6f, 0), Quaternion.Euler(0, 180, 0));
        m_meshGameObject.transform.parent = transform;

        m_renderer = transform.GetComponentInChildren<Renderer>();
        DarkenMaterial();

        m_animator = m_meshGameObject.GetComponent<Animator>();
        DOTween.Init(true, true);
    }

    public void DarkenMaterial()
    {
        m_renderer.material.color = new Color(0.2f, 0.2f, 0.2f);
    }

    public void RegularMaterial()
    {
        m_renderer.material.color = Color.white;
    }

    public void StartDestroyAnimation(Action p_callBack)
    {
        m_destroyCallBack = p_callBack;
        StartCoroutine(DestroyCoroutine());
    }

    public void StartEndGameAnimation() {
        m_animator.SetTrigger("Run");
        ParticleManager.instance.StartParticleFinish(transform.position);
    }

    private IEnumerator DestroyCoroutine()
    {
        //Sequence sequence = DOTween.Sequence();
        //sequence.Append(transform.Domo);
        m_meshGameObject.transform.DORotate(Vector3.forward, 0.35f);
        Vector3 pos1 = m_meshGameObject.transform.position + transform.forward * 3;
        Vector3 pos2 = pos1 + transform.right * 3;
        Vector3[] pathArr = new Vector3[] { pos1, pos2 };
        m_meshGameObject.transform.DOPath(pathArr, 0.65f, PathType.CatmullRom, PathMode.Full3D, 10).SetLookAt(0.1f, Vector3.forward, Vector3.up).SetDelay(0.4f);

        m_animator.SetTrigger("Run");
        yield return new WaitForSeconds(1);

        ParticleManager.instance.StartStar(m_meshGameObject.transform.position);
        if (!ReferenceEquals(m_destroyCallBack, null))
        {
            m_destroyCallBack();
        }
    }
}
