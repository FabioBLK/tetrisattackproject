﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    #region Singleton Definition
    private static GameController _instance;
    public static GameController Instance {
        get {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GameController>();
            }
            return _instance;
        }
    }
    #endregion

    #region Private variables
    [SerializeField]
    private BaseProperties m_baseProperties;
    private GameObject m_rowBlock;
    private bool m_matchPause = true;
    private bool m_uiPause = false;
    private PuzzleMatrix m_puzzleMatrix;
    private float m_matchPauseTime = 0;
    private int m_matchCount = 0;
    private int m_comboCount = 0;
    private int m_comboModeCount = 0;
    private int m_movesLeft = 0;
    private float m_rowInterval = 0;
    private Score m_score;
    private Vector3 m_previousRowPos = Vector3.zero;
    private RowSpeed m_rowSpeed;
    private PuzzleBox[] m_lastRow;
    #endregion

    #region Public variables
    public BaseProperties BaseProperties { get { return m_baseProperties; } }
    #endregion

    #region Events
    public event SetPause ChangePauseEvent;
    public event SetUIPause ChangeUIPauseEvent;
    public event SetInterval ChangeIntervalEvent;
    public event RowAdded AddRowEvent;
    public event UpdateUIData ChangeUIData;
    public event SetEndGame EndGameEvent;
    public event SetCompleteGame CompleteGameEvent;
    public event MoveOccured MoveOccuredEvent;
    #endregion

    #region Private Methods
    void Awake()
    {
        GameController findOther = FindObjectOfType<GameController>();
        if (findOther != null)
        {
            if (findOther != this)
                Destroy(this.gameObject);
        }
    }

    // Use this for initialization
    void Start () {
        Instantiate(m_baseProperties.SpawnerPrefab, Vector3.zero, Quaternion.identity);
        m_rowBlock = new GameObject("RowBlock");
        m_rowBlock.transform.position = Vector3.zero;
        m_rowInterval = m_baseProperties.RowTimeInterval;
        m_previousRowPos = m_baseProperties.SpawnPosition;
#if (UNITY_ANDROID || UNITY_IOS)
        GameObject swipeController = Instantiate(m_baseProperties.SwipeController, Vector3.zero, Quaternion.identity);
#else
        GameObject cursor3D = Instantiate(m_baseProperties.Cursor3D, Vector3.zero, Quaternion.identity);
        cursor3D.transform.parent = m_rowBlock.transform;
#endif

#if UNITY_EDITOR
        GameObject cursor3D = Instantiate(m_baseProperties.Cursor3D, Vector3.zero, Quaternion.identity);
        cursor3D.transform.parent = m_rowBlock.transform;
#endif
        m_puzzleMatrix = new PuzzleMatrix(m_baseProperties.RowsCount, m_baseProperties.ColumnsCount, m_baseProperties.MatchNumber, m_baseProperties.GameMode);

        m_score = new Score(
            m_baseProperties.SinglePointScore, 
            m_baseProperties.MatchNumber, 
            m_baseProperties.MultiplierScore, 
            m_baseProperties.ComboScore, 
            m_baseProperties.InitialScore,
            m_baseProperties.ScoreLevelThreshold, 
            m_baseProperties.MaxScoreLevelThreshold,
            m_baseProperties.ScoreMinBoost,
            m_baseProperties.ScoreMaxBoost);

        m_rowSpeed = new RowSpeed(
            m_baseProperties.MinRowTimeInterval,
            m_baseProperties.MaxRowTimeInterval,
            m_baseProperties.ScoreLevelThreshold,
            m_baseProperties.MaxScoreLevelThreshold,
            m_baseProperties.SpeedMaxBoost,
            m_baseProperties.SpeedMinBoost);

        for (int i = 0; i < m_baseProperties.RowsCount; i++)
        {
            GameObject emptyRow = new GameObject("EmptyRow");
            emptyRow.transform.parent = m_rowBlock.transform;
        }

        if(m_baseProperties.GameMode == GameModeType.Row)
        {
            SetupRowGame();
        }
        else if(m_baseProperties.GameMode == GameModeType.Combo)
        {
            SetupComboGame();
        }

        if(m_baseProperties.GameMode == GameModeType.Combo
            || m_baseProperties.GameMode == GameModeType.Level
            || m_baseProperties.GameMode == GameModeType.Row) {

            m_movesLeft = m_baseProperties.MovesAvailable;
        }

        //for (int i = 0; i < 100; i++) {
        //    float sboost = ExtraMath.CurvedLerp(i, 100, 1, -2);
        //    Debug.Log(sboost);
        //}

        //TODO - Remove this?
        Invoke("StartGame", 0.1f);
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.L))
        {
            m_rowInterval = m_rowInterval + 0.5f;
            DefineNewInterval(m_rowInterval);
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            m_rowInterval = m_rowInterval - 0.5f;
            DefineNewInterval(m_rowInterval);
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            Debug.Log("score = " + m_score.CurrentScore + " | level = " + m_score.GetCurrentLevel());
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            m_matchPause = !m_matchPause;
            MatchPauseGame(m_matchPause);
        }

        if (m_uiPause) {
            return;
        }

        if (!m_matchPause)
        {
            MoveRowBlock();
        }

        if (m_matchPauseTime > 0)
        {
            m_matchPauseTime -= Time.deltaTime;
            if(m_matchPauseTime <= 0)
            {
                MatchPauseGame(false);
                m_matchPauseTime = 0;
                m_matchCount = 0;
                m_comboCount = 0;
            }
        }

        if (Input.GetButtonDown("Fire2"))
        {
            //Application.Quit();
        }
	}

    private void MoveRowBlock()
    {
        float yPosition = 1 / m_rowInterval * Time.deltaTime;
        m_rowBlock.transform.position = new Vector3(m_rowBlock.transform.position.x, m_rowBlock.transform.position.y + yPosition, m_rowBlock.transform.position.z);
    }

    private void StartGame()
    {
        m_matchPause = false;
        ChangeIntervalEvent(m_rowInterval);
        ChangePauseEvent(m_matchPause);
        DefineNewInterval(m_rowSpeed.GetNewSpeed(m_score.CurrentScore));
    }

    private void MatchPauseGame(bool p_pause)
    {
        m_matchPause = p_pause;
        ChangePauseEvent(p_pause);
    }

    private void UIPause(bool p_pause) {
        m_uiPause = p_pause;
        ChangeUIPauseEvent(p_pause);
        ChangePauseEvent(p_pause);
    }

    private void DefineNewInterval(float p_interval)
    {
        m_rowInterval = p_interval;
        //Debug.LogWarning("New interval: " + m_rowInterval);
        ChangeIntervalEvent(m_rowInterval);
    }

    private void SetPuzzleBoxesColor(PuzzleBox[] p_boxArr)
    {
        for(int i = 0; i < p_boxArr.Length; i++)
        {
            if (!ReferenceEquals(p_boxArr[i], null))
            {
                p_boxArr[i].SetFadeColor(false);
            }
        }
    }

    private void SetupRowGame()
    {
        m_puzzleMatrix.BoxExistsInRow = RowGameIndexIsNull;
        m_puzzleMatrix.RowsGameFinishNumber = m_baseProperties.RowsToClear;
        GameObject rowGameIndicator = Instantiate(m_baseProperties.FinishGameIndicator);
        rowGameIndicator.transform.parent = m_rowBlock.transform;
        float indicatorYPos = m_rowBlock.transform.position.y - (m_baseProperties.RowsToClear) + (m_baseProperties.Cursor3DPosition.y) - 2;
        rowGameIndicator.transform.position = new Vector3(0, indicatorYPos, m_baseProperties.Cursor3DPosition.z + 0.15f);
    }

    private void SetupComboGame()
    {
        m_comboModeCount = m_baseProperties.CombosToClear;
    }

    private void RowGameIndexIsNull()
    {
        if (m_baseProperties.GameMode == GameModeType.Row) {
            Debug.LogWarning(string.Format("Level Complete. Star count = {0}", GetStarsCount()));
            if (CompleteGameEvent != null) {
                CompleteGameEvent(GetStarsCount());
            }
        }
        Debug.LogWarning("No blocks above destination row");
    }

    private void CheckComboModeCount()
    {
        Debug.LogWarning("Combos left = " + m_comboModeCount);
        if (m_baseProperties.GameMode == GameModeType.Combo)
        {
            if(m_comboModeCount <= 0)
            {
                Debug.LogWarning(string.Format("Level Complete. Star count = {0}", GetStarsCount()));
                if (CompleteGameEvent != null) {
                    CompleteGameEvent(GetStarsCount());
                }
            }
        }
    }

    private StarsCount GetStarsCount() {
        float movesPercent = (float)m_movesLeft / (float)m_baseProperties.MovesAvailable;
        StarsCount stars = StarsCount.One;
        if (movesPercent >= 0.5f) {
            stars = StarsCount.Three;
        }
        else if (movesPercent >= 0.25f) {
            stars = StarsCount.Two;
        }
        //Debug.LogWarning("stars = " + stars.ToString() + " | percent : " + movesPercent + " | mLeft: " + m_movesLeft + " | avlbl: " + m_baseProperties.MovesAvailable);
        return stars;
    }
#endregion

#region Public Methodos
    public void InsertRow(GameObject p_row, PuzzleBox[] p_boxArr)
    {
        p_row.transform.parent = m_rowBlock.transform;
        p_row.transform.localPosition = new Vector3(m_previousRowPos.x, m_previousRowPos.y - 1, m_previousRowPos.z);
        m_previousRowPos = p_row.transform.localPosition;
        if(m_lastRow != null)
        {
            SetPuzzleBoxesColor(m_lastRow);
            m_puzzleMatrix.InsertRow(m_lastRow);
        }
        m_lastRow = p_boxArr;
        if (!ReferenceEquals(AddRowEvent, null))
        {
            AddRowEvent();
        }
        
        //Fix deltatime offset on row position
        m_rowBlock.transform.position = new Vector3(m_rowBlock.transform.position.x, Mathf.Round(m_rowBlock.transform.position.y), m_rowBlock.transform.position.z);
    }

    public void InsertInitialRow(GameObject p_row, PuzzleBox[] p_boxArr, bool p_last)
    {
        p_row.transform.parent = m_rowBlock.transform;
        p_row.transform.localPosition = new Vector3(m_previousRowPos.x, m_previousRowPos.y - 1, m_previousRowPos.z);
        m_previousRowPos = p_row.transform.localPosition;

        if (!ReferenceEquals(AddRowEvent, null))
        {
            AddRowEvent();
        }

        if (!p_last)
        {
            SetPuzzleBoxesColor(p_boxArr);
        }
        
        m_puzzleMatrix.InsertRow(p_boxArr);

        m_rowBlock.transform.position = new Vector3(m_rowBlock.transform.position.x, m_rowBlock.transform.position.y + 1, m_rowBlock.transform.position.z);
    }

    public void PuzzleBoxClicked(BoxPosition p_box, MoveDirection p_dir)
    {
        m_puzzleMatrix.MovePuzzleBox(p_box, p_dir);
    }

    public bool PuzzleBoxExistsAt(BoxPosition p_pos)
    {
        return m_puzzleMatrix.BoxPositionExists(p_pos);
    }

    public void MatchPause(int p_matchValue, Vector3 p_firstBlockPosition)
    {
        MatchPauseGame(true);
        m_matchCount += p_matchValue;
        m_comboCount++;
        if (m_comboCount > 1)
        {
            SFXManager.instance.PlayCombo();
            ParticleManager.instance.StartCombo(p_firstBlockPosition);
            m_comboModeCount--;
            CheckComboModeCount();
            //TODO: Visual warning
        }
        m_score.AddMultiplePoints(p_matchValue, m_comboCount > 1);
        Debug.Log("Pause for " + m_matchPauseTime + " Seconds | Count = " + m_matchCount);
        p_matchValue = Mathf.Clamp(p_matchValue, 0, m_baseProperties.PauseTimer.Length);
        m_matchPauseTime += m_baseProperties.PauseTimer[p_matchValue];
        DefineNewInterval(m_rowSpeed.GetNewSpeed(m_score.CurrentScore));

        if(p_matchValue >= 4){
            SFXManager.instance.PlayMatchFour();
            ParticleManager.instance.StartExtraMatch(p_firstBlockPosition);
        }

        Debug.Log("Current Speed is " + m_rowSpeed.CurrentSpeed);

        if (!ReferenceEquals(ChangeUIData, null)) {
            ChangeUIData(m_score, m_comboModeCount);
        }

        if(m_baseProperties.GameMode == GameModeType.Level) {
            if (m_score.GetCurrentLevel() >= m_baseProperties.LevelsToClear) {
                Debug.LogWarning(string.Format("Level Complete. Star count = {0}", GetStarsCount()));
                if (CompleteGameEvent != null) {
                    CompleteGameEvent(GetStarsCount());
                }
            }
        }
    }

    public void ConfirmMoveOccured() {
        if (!ReferenceEquals(MoveOccuredEvent, null)) {
            m_movesLeft--;
            MoveOccuredEvent(m_movesLeft);
            if (m_movesLeft <= 0) {
                CallEndGame("MovesOver");
            }
        }
        SFXManager.instance.PlayPuppyMove();
    }

    public void SetBaseProperties(BaseProperties p_properties) {
        m_baseProperties = p_properties;
    }

    public void CallPauseGame() {
        UIPause(true);
    }

    public void CallUnpauseGame() {
        UIPause(false);
    }

    public void CallEndGame(string p_reason) {
        UIPause(true);
        //Call UI to show
        if(EndGameEvent != null) {
            EndGameEvent(p_reason);
        }
    }

    public void ShutDown() {
        GameObject spawner = FindObjectOfType<PuzzleSpawner>().gameObject;
        Destroy(spawner);

#if (UNITY_ANDROID || UNITY_IOS)
        GameObject swipeController = FindObjectOfType<SwipeController>().gameObject;
        Destroy(swipeController);
#endif

        Destroy(m_rowBlock);
        Destroy(this.gameObject);

    }
#endregion
}
