﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleSpawner : MonoBehaviour {

    #region private variables
    [SerializeField]
    private SpawnerProperties m_spawnerProperties;
    [SerializeField]
    private PuzzleBoxProperties[] m_puzzleBoxProperties;
    private float m_timer = 0;
    private float m_interval;
    private int m_columnCount;
    private int m_rowCount;
    private bool m_pause = true;
    private bool m_started = false;
    private int[] m_upperProperties;
    private int leftProperty = 0;
    #endregion

    #region public variables

    #endregion

    #region public methods
    public void SetPause(bool p_value)
    {
        if (!m_started)
        {
            GenerateStartBoxes();
            m_started = true;
        }
        m_pause = p_value;
    }

    public void SetInterval(float p_interval)
    {
        m_interval = p_interval;
    }
    #endregion

    #region private methods
    // Use this for initialization
    private void Start ()
    {
        GameController.Instance.ChangePauseEvent += SetPause;
        GameController.Instance.ChangeIntervalEvent += SetInterval;
        m_columnCount = GameController.Instance.BaseProperties.ColumnsCount;
        m_rowCount = GameController.Instance.BaseProperties.RowsCount;

        m_upperProperties = new int[m_columnCount];
        for(int i = 0;i< m_upperProperties.Length; i++)
        {
            m_upperProperties[i] = 0;
        }
    }
	
	// Update is called once per frame
	private void Update () {
        if (!m_pause)
        {
            m_timer += Time.deltaTime;

            if (m_timer >= m_interval)
            {
                Spawn();
                m_timer = 0;
            }
        }
	}

    private void Spawn()
    {
        GameObject row = new GameObject("Row");
        row.transform.position = Vector3.zero;

        PuzzleBox[] puzzleBoxes = PuzzleBoxes(row.transform);

        GameController.Instance.InsertRow(row, puzzleBoxes);

    }

    private PuzzleBox[] PuzzleBoxes(Transform p_parent)
    {
        PuzzleBox[] puzzleBoxes = new PuzzleBox[m_columnCount];

        for (int i = 0; i < m_columnCount; i++)
        {
            GameObject box = Instantiate(m_spawnerProperties.BoxPrefab, Vector3.zero, Quaternion.identity);
            box.transform.parent = p_parent;
            float sideDistance = m_spawnerProperties.SideDistance * i;
            box.transform.localPosition = new Vector3(sideDistance, 0, 0);

            int random = RandomProperty(leftProperty, m_upperProperties[i]);
            puzzleBoxes[i] = new PuzzleBox(box, m_puzzleBoxProperties[random], GameController.Instance.BaseProperties);
            leftProperty = random;
            m_upperProperties[i] = random;
        }

        return puzzleBoxes;
    }

    private void GenerateStartBoxes()
    {
        int reservedColumn = Random.Range(0, m_columnCount);
        int rowCount = m_rowCount / 2;

        for(int i = 0; i < rowCount; i++)
        {
            GameObject row = new GameObject("Row");
            row.transform.position = Vector3.zero;

            PuzzleBox[] puzzleBoxes = PuzzleBoxes(row.transform);
            puzzleBoxes[reservedColumn].DestroyPuzzleBox();
            puzzleBoxes[reservedColumn] = null;
            if (i == 0)
            {
                for (int j = 0; j < puzzleBoxes.Length; j++)
                {
                    int create = Random.Range(0, 2);
                    if (create == 0)
                    {
                        if (!ReferenceEquals(puzzleBoxes[j], null))
                        {
                            puzzleBoxes[j].DestroyPuzzleBox();
                            puzzleBoxes[j] = null;
                        }
                    }
                }
            }

            bool last = i == rowCount;

            GameController.Instance.InsertInitialRow(row, puzzleBoxes, last);
        }
    }

    private int RandomProperty(int p_leftProperty, int p_upProperty)
    {
        int result;
        do
        {
            result = Random.Range(0, m_puzzleBoxProperties.Length);
        }
        while (result == p_leftProperty || result == p_upProperty);
       
        return result;
    }

    #endregion
}
