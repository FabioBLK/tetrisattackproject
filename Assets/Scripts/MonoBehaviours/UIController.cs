﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DoozyUI;
using UnityEngine.UI;
using System;
using TMPro;
using UnityEngine.Events;

public class UIController : MonoBehaviour {

    private Stages m_stages;
    private Button m_infoButton;
    private Button m_pauseButton;
    private Button m_continueButton;
    private Button m_restartButton;
    private Button m_quitButton;
    private Button m_endRestartButton;
    private Button m_endQuitButton;
    private TMP_Text m_scoreText;
    private TMP_Text m_levelText;
    private TMP_Text m_combosText;
    private TMP_Text m_movesText;
    private Animator m_movesTextAnim;
    private Animator m_combosTextAnim;
    private Animator m_levelsTextAnim;
    private Slider m_levelSlider;
    private Slider m_combosSlider;
    private Transform m_masterCanvasTransform;
    private LevelInfo m_lastLoadedLevel;
    private TranslationStrings m_translation;
    private Transform m_levelSelectMenu;
    private List<Button> m_selectStageButtons = new List<Button>();
    private List<Animator> m_selectStageButtonsAnim = new List<Animator>();
    private Button m_nextStageSelectButton;
    private Button m_previousStageSelectButton;
    private Button m_backToIntroButton;
    private Button m_startGameButton;
    private Button m_musicButton;
    private Button m_sfxButton;
    private TMP_Text m_levelsMenuText;
    private int m_currentButtonId = 0;
    private int m_lastComboCount = 0;
    private int m_lastLevelCount = 0;
    private bool m_stagesMenuOpen = false;
    private bool m_pauseTimescale = false;

    [SerializeField]
    private GameObject m_gameController;
    [SerializeField]
    private GameObject m_stageButton;

    private BaseProperties m_properties;
    private AdsRequester m_adsRequester;

    private static int MAX_BUTTON_COUNT = 12;
    private int m_selectSceneNumber = 0;
    private int m_currentSceneCanvas = 0;

	// Use this for initialization
	void Start () {

        Translation translation = new Translation(Application.systemLanguage);
        m_translation = translation.CurrentTranslation;

        m_stages = new Stages();

        m_adsRequester = new AdsRequester();

        m_masterCanvasTransform = UIManager.GetMasterCanvas().transform;

        FillIntroMenuButton();

        m_levelSelectMenu = m_masterCanvasTransform.Find("Levels_Menu/Background/StagePanel");

        FillStageSelectButtons();

        //Levels Menu Translation
        m_currentSceneCanvas = (m_selectSceneNumber / m_stages.StageGroup.levelGroup.Count) + 1;
        int totalPages = (m_stages.StageGroup.levelGroup.Count / MAX_BUTTON_COUNT) + 1;
        m_levelsMenuText = m_masterCanvasTransform.Find("Levels_Menu/Background/Title/Text").GetComponent<TMP_Text>();
        m_levelsMenuText.text = string.Format("{0} {1}/{2}", m_translation.stages, m_currentSceneCanvas, totalPages);
        Invoke("CallIntroScreen", 1);

        MusicManager.instance.ChangeMusic(MusicManager.MusicList.OpeningScene);
    }

    private void FillIntroMenuButton() {
        m_startGameButton = m_masterCanvasTransform.Find("Intro_Menu/Background/StartGame_Button/Button").GetComponent<Button>();
        m_startGameButton.onClick.AddListener(ShowGameModeNotification);

        m_musicButton = m_masterCanvasTransform.Find("Intro_Menu/Background/Music_Button/Button").GetComponent<Button>();
        m_musicButton.onClick.AddListener(MuteMusicButton);

        m_sfxButton = m_masterCanvasTransform.Find("Intro_Menu/Background/SFX_Button/Button").GetComponent<Button>();
        m_sfxButton.onClick.AddListener(MuteSFXButton);

        ChangeSFXButton();
        ChangeMusicButton();
    }

    private void ChangeSFXButton() {
        if (!SFXManager.instance.IsSFXMute()) {
            m_sfxButton.transform.GetChild(0).gameObject.SetActive(true);
            m_sfxButton.transform.GetChild(1).gameObject.SetActive(false);
        }
        else {
            m_sfxButton.transform.GetChild(0).gameObject.SetActive(false);
            m_sfxButton.transform.GetChild(1).gameObject.SetActive(true);
        }
    }

    private void ChangeMusicButton() {
        if (!MusicManager.instance.IsMusicMute()) {
            m_musicButton.transform.GetChild(0).gameObject.SetActive(true);
            m_musicButton.transform.GetChild(1).gameObject.SetActive(false);
        }
        else {
            m_musicButton.transform.GetChild(0).gameObject.SetActive(false);
            m_musicButton.transform.GetChild(1).gameObject.SetActive(true);
        }
    }

    private void FillStageSelectButtons() {
        int stageCount = m_stages.StageGroup.levelGroup.Count;
        for (int i = m_selectSceneNumber; (i < stageCount) && (i < m_selectSceneNumber + MAX_BUTTON_COUNT); i++) {
            GameObject stageButtonUI = Instantiate(m_stageButton);
            stageButtonUI.transform.SetParent(m_levelSelectMenu);
            m_selectStageButtons.Add(stageButtonUI.GetComponent<Button>());
            m_selectStageButtonsAnim.Add(m_selectStageButtons[i].GetComponent<Animator>());
            SetButtonStars(m_selectStageButtons[i], m_stages.Progress.levels[i].stars);
            LevelInfo info = m_stages.StageGroup.levelGroup[i];
            int buttonId = i;
            m_selectStageButtons[i].onClick.AddListener(() => StartLevel(info, buttonId));
            TMP_Text stageText = stageButtonUI.transform.Find("LevelNumber").GetComponent<TMP_Text>();
            stageText.text = info.levelNumber.ToString();
        }

        m_nextStageSelectButton = m_masterCanvasTransform.Find("Levels_Menu/Background/NextButton").GetComponent<Button>();
        m_nextStageSelectButton.onClick.AddListener(() => NextStageSelect(true));

        m_previousStageSelectButton = m_masterCanvasTransform.Find("Levels_Menu/Background/PreviousButton").GetComponent<Button>();
        m_previousStageSelectButton.onClick.AddListener(() => NextStageSelect(false));

        m_backToIntroButton = m_masterCanvasTransform.Find("Levels_Menu/Background/BackToIntroButton").GetComponent<Button>();
        m_backToIntroButton.onClick.AddListener(BackFromStageToIntro);

        m_previousStageSelectButton.interactable = false;
    }

    private void ShowGameModeNotification() {
        if(Time.timeScale < 1) {
            Time.timeScale = 1;
        }
        m_startGameButton.interactable = false;
        SFXManager.instance.PlayUIClick();
        string[] buttonNames = new string[] { "EndlessBtn", "StagesBtn" };
        string[] buttonText = new string[] { m_translation.endless_mode, m_translation.stages_mode };
        UnityAction[] buttonActions = new UnityAction[] { CallEndlessGame, FromIntroToStageSelect };
        UIManager.ShowNotification("UINotification_GameMode", -1, false, m_translation.select_mode, buttonNames, buttonText, buttonActions);
        AnalyticsUpdater.RegisterScreenVisit(UnityEngine.Analytics.ScreenName.MainMenu);
    }

    private void SetButtonStars(Button p_button, int p_stars) {
        p_button.transform.GetChild(2).gameObject.SetActive(false);
        p_button.transform.GetChild(3).gameObject.SetActive(false);
        p_button.transform.GetChild(4).gameObject.SetActive(false);

        if (p_stars == 1) {
            p_button.transform.GetChild(4).gameObject.SetActive(true);
        }
        else if (p_stars == 2) {
            p_button.transform.GetChild(2).gameObject.SetActive(true);
            p_button.transform.GetChild(3).gameObject.SetActive(true);
        }
        else if (p_stars == 3) {
            p_button.transform.GetChild(2).gameObject.SetActive(true);
            p_button.transform.GetChild(3).gameObject.SetActive(true);
            p_button.transform.GetChild(4).gameObject.SetActive(true);
        }
    }

    private void NextStageSelect(bool p_next) {
        m_nextStageSelectButton.interactable = false;
        m_previousStageSelectButton.interactable = false;
        SFXManager.instance.PlayUIClick();
        int firstButtonIndex = 0;
        for(int i = 0; i < m_selectStageButtons.Count; i++) {
            m_selectStageButtons[i].onClick.RemoveAllListeners();
            if (m_selectStageButtons[i].interactable) {
                m_selectStageButtons[i].interactable = false;
                m_selectStageButtonsAnim[i].SetTrigger("Out");
                firstButtonIndex = i;
            }
        }

        StartCoroutine(RefreshButtons(p_next, m_selectStageButtonsAnim[firstButtonIndex]));
    }

    private IEnumerator RefreshButtons(bool p_next, Animator p_runningAnim) {

        yield return new WaitUntil(() => p_runningAnim.GetCurrentAnimatorStateInfo(0).normalizedTime < 1.0f);

        int stageCount = m_stages.StageGroup.levelGroup.Count;
        int buttonCount = 0;

        if (p_next) {
            m_selectSceneNumber += MAX_BUTTON_COUNT;
        }
        else {
            m_selectSceneNumber -= MAX_BUTTON_COUNT;
        }

        m_currentSceneCanvas = (m_selectSceneNumber / MAX_BUTTON_COUNT) + 1;
        int totalPages = (m_stages.StageGroup.levelGroup.Count / MAX_BUTTON_COUNT) + 1;
        m_levelsMenuText.text = string.Format("{0} {1}/{2}", m_translation.stages, m_currentSceneCanvas, totalPages);

        for (int i = m_selectSceneNumber; (i < stageCount) && (i < m_selectSceneNumber + MAX_BUTTON_COUNT); i++) {
            LevelInfo info = m_stages.StageGroup.levelGroup[i];
            int buttonId = buttonCount;
            m_selectStageButtons[buttonCount].onClick.AddListener(() => StartLevel(info, buttonId));
            TMP_Text stageText = m_selectStageButtons[buttonCount].transform.Find("LevelNumber").GetComponent<TMP_Text>();
            stageText.text = info.levelNumber.ToString();
            m_selectStageButtons[buttonCount].interactable = true;
            SetButtonStars(m_selectStageButtons[buttonCount], m_stages.Progress.levels[i].stars);
            m_selectStageButtonsAnim[buttonCount].SetTrigger("In");
            buttonCount++;
        }

        if (p_next) {
            if ((m_selectSceneNumber + MAX_BUTTON_COUNT) >= stageCount) {
                StartCoroutine(ActivateNextPreviousButtons(false, true));
            }
            else {
                StartCoroutine(ActivateNextPreviousButtons(true, true));
            }
        }
        else {
            if ((m_selectSceneNumber - MAX_BUTTON_COUNT) < 0) {
                StartCoroutine(ActivateNextPreviousButtons(true, false));
            }
            else {
                StartCoroutine(ActivateNextPreviousButtons(true, true));
            }
        }
    }

    private IEnumerator ActivateNextPreviousButtons(bool p_next, bool p_previous) {
        yield return new WaitForSeconds(0.35f);

        m_nextStageSelectButton.interactable = p_next;
        m_previousStageSelectButton.interactable = p_previous;
    }

    // Update is called once per frame
    void Update () {
        if (m_pauseTimescale && Time.timeScale < 1) {

            int musicEnabledInt = PlayerPrefs.GetInt("Music");
            bool musicEnabled = (musicEnabledInt == 0);
            if (musicEnabled)
            {
                MusicManager.instance.MuteMusic(false, false);
            }

            Time.timeScale = 1;
        }
        if (Input.GetKeyDown(KeyCode.L)) {
            Time.timeScale = 1;
        }
	}

    private void StartLevel(LevelInfo p_level, int p_buttonId, bool p_showObjective = true) {
        
        //Dont start if there`s an instance of gamecontroller.
        if (!ReferenceEquals(GameController.Instance, null)) {
            return;
        }

        AnalyticsUpdater.RegisterLevelStart(p_level);
        m_adsRequester.RequestBanner();
        m_adsRequester.RequestInterstitial();

        m_stagesMenuOpen = false;

        MusicManager.instance.ChangeMusic(MusicManager.MusicList.MainMusic);
        SFXManager.instance.PlayUIEnter(0.2f);
        m_lastLoadedLevel = p_level;
        m_currentButtonId = p_buttonId;
        UIManager.HideUiElement("Levels_Menu");

        GameObject controllerGO = Instantiate(m_gameController);
        GameController controller = controllerGO.GetComponent<GameController>();
        m_properties = Resources.Load<BaseProperties>(string.Format("level_properties/{0}", p_level.controllerName));
        controller.SetBaseProperties(m_properties);

        controller.ChangeUIData += UpdateUI;
        controller.EndGameEvent += EndGameUI;
        controller.CompleteGameEvent += CompleteGameUI;
        if(m_properties.GameMode != GameModeType.Endless) {
            controller.MoveOccuredEvent += UpdateMovesUI;
        }

        List<string> uiElements = UINames.GameElementsList(m_properties.GameMode);
        for (int i = 0; i < uiElements.Count; i++) {
            UIManager.ShowUiElement(uiElements[i]);
        }

        UIElement buttonsElement = m_masterCanvasTransform.Find(string.Format("{0}ButtonsElement", UINames.GetShortNameMode(m_properties.GameMode))).GetComponent<UIElement>();
        buttonsElement.OnInAnimationsFinish.RemoveAllListeners();
        if (p_showObjective) {
            buttonsElement.OnInAnimationsFinish.AddListener(ShowObjective);
        }

        m_infoButton = m_masterCanvasTransform.Find(string.Format("{0}ButtonsElement/GameTypeButton", UINames.GetShortNameMode(m_properties.GameMode))).GetComponent<Button>();
        m_infoButton.onClick.RemoveAllListeners();
        m_infoButton.onClick.AddListener(ShowObjective);

        m_pauseButton = m_masterCanvasTransform.Find(string.Format("{0}ButtonsElement/PauseUI", UINames.GetShortNameMode(m_properties.GameMode))).GetComponent<Button>();
        m_pauseButton.onClick.RemoveAllListeners();
        m_pauseButton.onClick.AddListener(PauseMenu);

        //GameType Button Translation
        TMP_Text infoButtonText = m_masterCanvasTransform.Find(string.Format("{0}ButtonsElement/GameTypeButton/Label TMPro", UINames.GetShortNameMode(m_properties.GameMode))).GetComponent<TMP_Text>();
        infoButtonText.text = m_translation.row_type;

        //Pause Menu Buttons
        m_continueButton = m_masterCanvasTransform.Find("Pause_Menu/ContinueElement/ContinueBtn").GetComponent<Button>();
        m_continueButton.onClick.RemoveAllListeners();
        m_continueButton.onClick.AddListener(ContinueGame);

        m_restartButton = m_masterCanvasTransform.Find("Pause_Menu/RestartElement/RestartBtn").GetComponent<Button>();
        m_restartButton.onClick.RemoveAllListeners();
        m_restartButton.onClick.AddListener(PrepareRestartGame);

        m_quitButton = m_masterCanvasTransform.Find("Pause_Menu/QuitElement/QuitBtn").GetComponent<Button>();

        //Pause Menu Translations
        TMP_Text continueButtonText = m_masterCanvasTransform.Find("Pause_Menu/ContinueElement/ContinueBtn/Label TMPro").GetComponent<TMP_Text>();
        continueButtonText.text = m_translation.continue_btn;

        TMP_Text restartButtonText = m_masterCanvasTransform.Find("Pause_Menu/RestartElement/RestartBtn/Label TMPro").GetComponent<TMP_Text>();
        restartButtonText.text = m_translation.restart_btn;

        TMP_Text quitButtonText = m_masterCanvasTransform.Find("Pause_Menu/QuitElement/QuitBtn/Label TMPro").GetComponent<TMP_Text>();
        quitButtonText.text = m_translation.quit_btn;

        //GameOver Menu Buttons
        m_endRestartButton = m_masterCanvasTransform.Find("GameOver_Menu/RestartElement/RestartBtn").GetComponent<Button>();
        m_endRestartButton.onClick.RemoveAllListeners();
        m_endRestartButton.onClick.AddListener(PrepareRestartGame);

        m_endQuitButton = m_masterCanvasTransform.Find("GameOver_Menu/QuitElement/QuitBtn").GetComponent<Button>();
        if(m_properties.GameMode == GameModeType.Endless) {
            m_quitButton.onClick.RemoveAllListeners();
            m_quitButton.onClick.AddListener(QuitGameFromEndless);

            m_endQuitButton.onClick.RemoveAllListeners();
            m_endQuitButton.onClick.AddListener(QuitGameFromEndless);
        }
        else {
            m_quitButton.onClick.RemoveAllListeners();
            m_quitButton.onClick.AddListener(QuitGame);

            m_endQuitButton.onClick.RemoveAllListeners();
            m_endQuitButton.onClick.AddListener(QuitGame);
        }

        //GameOver Menu Translations
        TMP_Text gameOverText = m_masterCanvasTransform.Find("GameOver_Menu/GameOverElement/Background/TextMeshPro Text").GetComponent<TMP_Text>();
        gameOverText.text = m_translation.game_over;

        TMP_Text endRestartButtonText = m_masterCanvasTransform.Find("GameOver_Menu/RestartElement/RestartBtn/Label TMPro").GetComponent<TMP_Text>();
        endRestartButtonText.text = m_translation.restart_btn;

        TMP_Text endQuitButtonText = m_masterCanvasTransform.Find("GameOver_Menu/QuitElement/QuitBtn/Label TMPro").GetComponent<TMP_Text>();
        endQuitButtonText.text = m_translation.quit_btn;

        //Score UI
        m_scoreText = m_masterCanvasTransform.Find(string.Format("{0}Items/ScoreLabel/ScoreText", UINames.GetShortNameMode(m_properties.GameMode))).GetComponent<TMP_Text>();
        m_scoreText.text = GetFormatedScore(0);

        if(m_properties.GameMode != GameModeType.Endless) {
            m_movesText = m_masterCanvasTransform.Find(string.Format("{0}Items/MovesGroup/Moves", UINames.GetShortNameMode(m_properties.GameMode))).GetComponent<TMP_Text>();
            m_movesText.text = m_properties.MovesAvailable.ToString();

            m_movesTextAnim = m_masterCanvasTransform.Find(string.Format("{0}Items/MovesGroup", UINames.GetShortNameMode(m_properties.GameMode))).GetComponent<Animator>();

            //Score UI Translation
            TMP_Text movesLabelText = m_masterCanvasTransform.Find(string.Format("{0}Items/MovesGroup/Label", UINames.GetShortNameMode(m_properties.GameMode))).GetComponent<TMP_Text>();
            movesLabelText.text = m_translation.moves;
        }

        if (m_properties.GameMode == GameModeType.Level || m_properties.GameMode == GameModeType.Endless) {
            m_levelText = m_masterCanvasTransform.Find(string.Format("{0}Items/LevelGroup/Level", UINames.GetShortNameMode(m_properties.GameMode))).GetComponent<TMP_Text>();
            m_levelText.text = "0";

            m_levelsTextAnim = m_masterCanvasTransform.Find(string.Format("{0}Items/LevelGroup", UINames.GetShortNameMode(m_properties.GameMode))).GetComponent<Animator>();

            TMP_Text levelLabelText = m_masterCanvasTransform.Find(string.Format("{0}Items/LevelGroup/Label", UINames.GetShortNameMode(m_properties.GameMode))).GetComponent<TMP_Text>();
            levelLabelText.text = m_translation.level;

            m_levelSlider = m_masterCanvasTransform.Find(string.Format("{0}Items/Slider", UINames.GetShortNameMode(m_properties.GameMode))).GetComponent<Slider>();
            m_levelSlider.value = 0;

            if (m_properties.GameMode == GameModeType.Level) {
                infoButtonText.text = m_translation.level_type;
            }
            else {
                infoButtonText.text = m_translation.endless_type;
            }
            
        }
        else if (m_properties.GameMode == GameModeType.Combo) {
            m_combosText = m_masterCanvasTransform.Find(string.Format("{0}Items/CombosGroup/Combos", UINames.GetShortNameMode(m_properties.GameMode))).GetComponent<TMP_Text>();
            m_combosText.text = m_properties.CombosToClear.ToString();

            m_combosTextAnim = m_masterCanvasTransform.Find(string.Format("{0}Items/CombosGroup", UINames.GetShortNameMode(m_properties.GameMode))).GetComponent<Animator>();

            TMP_Text combosLabelText = m_masterCanvasTransform.Find(string.Format("{0}Items/CombosGroup/Label", UINames.GetShortNameMode(m_properties.GameMode))).GetComponent<TMP_Text>();
            combosLabelText.text = m_translation.combos;

            m_combosSlider = m_masterCanvasTransform.Find(string.Format("{0}Items/Slider", UINames.GetShortNameMode(m_properties.GameMode))).GetComponent<Slider>();
            m_combosSlider.maxValue = m_properties.CombosToClear;

            infoButtonText.text = m_translation.combo_type;
        }
    }

    private void CompleteGameUI(StarsCount p_stars) {
        AnalyticsUpdater.RegisterLevelComplete(m_lastLoadedLevel, p_stars);
        m_stages.UpdateProgress(m_lastLoadedLevel.id, true, p_stars);
        SetButtonStars(m_selectStageButtons[m_currentButtonId], (int)p_stars + 1);
        ShowCompleteGame(p_stars);
        m_adsRequester.ShowBanner();
    }

    private void EndGameUI(string p_reason) {
        AnalyticsUpdater.RegisterLevelFailed(m_lastLoadedLevel, p_reason);
        SFXManager.instance.PlayEndGame();
        Debug.LogWarning("End Game");
        UIManager.ShowUiElement("GameOver_Menu");
        UIManager.ShowUiElement("GameOver_Buttons");
        m_adsRequester.ShowBanner();
    }

    private void ContinueGame() {
        UIManager.HideUiElement("Pause_Buttons");
        UIManager.HideUiElement("Pause_Menu");
        SFXManager.instance.PlayUIClick();
        UnpauseGame();
    }

    private void QuitGame() {
        AnalyticsUpdater.RegisterLevelQuit(m_lastLoadedLevel);
        MusicManager.instance.ChangeMusic(MusicManager.MusicList.OpeningScene);
        SFXManager.instance.PlayUIClick();
        UIManager.HideUiElement("GameOver_Buttons");
        UIManager.HideUiElement("GameOver_Menu");

        UIManager.HideUiElement("Pause_Buttons");
        UIManager.HideUiElement("Pause_Menu");

        List<string> uiElements = UINames.GameElementsList(m_properties.GameMode);
        for (int i = 0; i < uiElements.Count; i++) {
            if (i == uiElements.Count - 1) {
                UIElement element = UIManager.GetUiElements(uiElements[0])[0];
                element.OnOutAnimationsFinish.RemoveAllListeners();
                element.OnOutAnimationsFinish.AddListener(()=> {
                    GameController.Instance.ShutDown();
                    UIManager.ShowUiElement("Levels_Menu");
                });
            }
            UIManager.HideUiElement(uiElements[i]);
        }
        m_adsRequester.HideBanner();

        int musicEnabledInt = PlayerPrefs.GetInt("Music");
        bool musicEnabled = (musicEnabledInt == 0);
        if (musicEnabled)
        {
            MusicManager.instance.MuteMusic(true, false);
        }

        Time.timeScale = 0;
        m_pauseTimescale = false;
        m_adsRequester.ShowInterstitial(ResumeGameCallBack);
    }

    private void QuitGameFromEndless() {
        SFXManager.instance.PlayUIClick();
        UIManager.HideUiElement("GameOver_Buttons");
        UIManager.HideUiElement("GameOver_Menu");

        UIManager.HideUiElement("Pause_Buttons");
        UIManager.HideUiElement("Pause_Menu");

        List<string> uiElements = UINames.GameElementsList(m_properties.GameMode);
        for (int i = 0; i < uiElements.Count; i++) {
            if (i == uiElements.Count - 1) {
                UIElement element = UIManager.GetUiElements(uiElements[0])[0];
                element.OnOutAnimationsFinish.RemoveAllListeners();
                element.OnOutAnimationsFinish.AddListener(() => {
                    GameController.Instance.ShutDown();
                    UIManager.ShowUiElement("Intro_Menu");
                    MusicManager.instance.ChangeMusic(MusicManager.MusicList.OpeningScene);
                });
            }
            UIManager.HideUiElement(uiElements[i]);
        }
        m_adsRequester.HideBanner();

        int musicEnabledInt = PlayerPrefs.GetInt("Music");
        bool musicEnabled = (musicEnabledInt == 0);
        if (musicEnabled)
        {
            MusicManager.instance.MuteMusic(true, false);
        }

        Time.timeScale = 0;
        m_pauseTimescale = false;
        m_adsRequester.ShowInterstitial(ResumeGameCallBack);
    }

    private void ResumeGameCallBack() {
        Debug.LogWarning("revert timescale");
        m_pauseTimescale = true;
    }

    private void PrepareRestartGame() {
        SFXManager.instance.PlayUIClick();
        UIManager.HideUiElement("GameOver_Buttons");
        UIManager.HideUiElement("GameOver_Menu");

        UIManager.HideUiElement("Pause_Buttons");
        UIManager.HideUiElement("Pause_Menu");

        GameController.Instance.ShutDown();

        List<string> uiElements = UINames.GameElementsList(m_properties.GameMode);
        for (int i = 0; i < uiElements.Count; i++) {
            if (i == uiElements.Count - 1) {
                UIElement element = UIManager.GetUiElements(uiElements[0])[0];
                element.OnOutAnimationsFinish.RemoveAllListeners();
                element.OnOutAnimationsFinish.AddListener(RestartGame);
            }
            UIManager.HideUiElement(uiElements[i]);
        }

        m_adsRequester.HideBanner();
    }

    private void RestartGame() {
        
        StartLevel(m_lastLoadedLevel, m_currentButtonId, false);
    }

    private void PauseMenu() {
        SFXManager.instance.PlayUIClick();
        GameController.Instance.CallPauseGame();
        UIManager.ShowUiElement("Pause_Menu");
        List<UIElement> elements = UIManager.GetUiElements("Pause_Menu");
        elements[0].OnInAnimationsFinish.RemoveAllListeners();
        elements[0].OnInAnimationsFinish.AddListener(ShowPauseMenuButtons);
        m_adsRequester.ShowBanner();
    }

    private void ShowPauseMenuButtons() {
        UIManager.ShowUiElement("Pause_Buttons");
    }

    private void UpdateUI(Score p_score, int p_combosLeft) {
        if(m_properties.GameMode == GameModeType.Endless || m_properties.GameMode == GameModeType.Level) {
            m_scoreText.text = GetFormatedScore(p_score.CurrentScore);
            m_levelText.text = p_score.GetCurrentLevel().ToString();
            m_levelSlider.value = p_score.GetDecimalToNextLevel();
            int currentLevel = p_score.GetCurrentLevel();
            if (currentLevel > m_lastLevelCount) {
                m_levelsTextAnim.SetTrigger("animate");
            }

            m_lastLevelCount = p_score.GetCurrentLevel();
        }
        else if(m_properties.GameMode == GameModeType.Row) {
            m_scoreText.text = GetFormatedScore(p_score.CurrentScore);
        }
        else if(m_properties.GameMode == GameModeType.Combo) {
            m_scoreText.text = GetFormatedScore(p_score.CurrentScore);
            m_combosText.text = p_combosLeft.ToString();
            m_combosSlider.value = m_combosSlider.maxValue - p_combosLeft;
            if(p_combosLeft < m_lastComboCount) {
                m_combosTextAnim.SetTrigger("animate");
            }
            m_lastComboCount = p_combosLeft;
        }
    }

    private void UpdateMovesUI(int m_movesLeft) {
        m_movesText.text = m_movesLeft.ToString();
        m_movesTextAnim.SetTrigger("animate");
    }

    private void ShowObjective() {
        string title = string.Empty;
        string info = string.Empty;

        if (m_properties.GameMode == GameModeType.Combo) {
            title = m_translation.combo_mode;
            string objectiveText = string.Format("{0}<color=#D53811><size=150%>{1}<color=#000000><size=100%>{2}", m_translation.combo_info_begin, m_properties.CombosToClear, m_translation.combo_info_end);
            string movesText = string.Format("{0}<color=#D53811><size=150%>{1}<color=#000000><size=100%>{2}", m_translation.moves_left_begin, m_properties.MovesAvailable, m_translation.moves_left_end);
            info = string.Format("{0}\n{1}", objectiveText, movesText);
        }
        else if(m_properties.GameMode == GameModeType.Endless) {
            title = m_translation.endless_mode;
            info = m_translation.endless_info;
        }
        else if (m_properties.GameMode == GameModeType.Level) {
            title = m_translation.level_mode;
            string movesText = string.Format("{0}<color=#D53811><size=150%>{1}<color=#000000><size=100%>{2}", m_translation.moves_left_begin, m_properties.MovesAvailable, m_translation.moves_left_end);
            string objectiveText = string.Format("{0}<color=#D53811><size=150%>{1}<color=#000000><size=100%>", m_translation.level_info, m_properties.LevelsToClear);
            info = string.Format("{0}\n{1}", objectiveText, movesText);
        }
        else if (m_properties.GameMode == GameModeType.Row) {
            title = m_translation.row_mode;
            string movesText = string.Format("{0}<color=#D53811><size=150%>{1}<color=#000000><size=100%>{2}", m_translation.moves_left_begin, m_properties.MovesAvailable, m_translation.moves_left_end);
            string objectiveText = string.Format("{0}<color=#D53811><size=150%>{1}<color=#000000><size=100%>{2}", m_translation.row_info_begin, m_properties.RowsToClear, m_translation.row_info_end);
            info = string.Format("{0}\n{1}", objectiveText, movesText);
        }

        GameController.Instance.CallPauseGame();
        UIManager.ShowNotification("UINotification", -1, false, title, info, UnpauseGame);
        SFXManager.instance.PlayUIClick();
        m_adsRequester.ShowBanner();
    }

    private void ShowCompleteGame(StarsCount p_stars) {
        GameController.Instance.CallPauseGame();
        if(p_stars == StarsCount.Three) {
            UIManager.ShowNotification("UINotification_Star3", -1, false, m_translation.congratulations, m_translation.stage_clear, QuitGame);
        }else if (p_stars == StarsCount.Two) {
            UIManager.ShowNotification("UINotification_Star2", -1, false, m_translation.congratulations, m_translation.stage_clear, QuitGame);
        }
        else {
            UIManager.ShowNotification("UINotification_Star1", -1, false, m_translation.congratulations, m_translation.stage_clear, QuitGame);
        }
        SFXManager.instance.PlayCompleteGame();
        
    }

    private void UnpauseGame() {
        m_adsRequester.HideBanner();
        GameController.Instance.CallUnpauseGame();
    }

    private void CallIntroScreen() {
        UIManager.ShowUiElement("Intro_Menu");
        SFXManager.instance.PlayUIEnter(1);
        UIManager.HideUiElement("Intro_Background");
        AnalyticsUpdater.RegisterScreenVisit(UnityEngine.Analytics.ScreenName.Title);
    }

    private void BackFromStageToIntro() {
        UIManager.HideUiElement("Levels_Menu");
        UIManager.ShowUiElement("Intro_Menu");
        SFXManager.instance.PlayUIClick();
        SFXManager.instance.PlayUIEnter(1);
    }

    private void CallStageSelect() {
        //Call stage select menu
        m_stagesMenuOpen = false;
        UIManager.ShowUiElement("Levels_Menu");
        SFXManager.instance.PlayUIEnter();
        AnalyticsUpdater.RegisterScreenVisit(UnityEngine.Analytics.ScreenName.Map);
    }

    private void FromIntroToStageSelect() {
        m_stagesMenuOpen = true;
        if (!ReferenceEquals(GameController.Instance, null)) {
            return;
        }
        m_startGameButton.interactable = true;
        SFXManager.instance.PlayUIClick();
        UIManager.HideUiElement("Intro_Menu");
        Invoke("CallStageSelect", 0.5f);
    }

    private void CallEndlessGame() {
        if (m_stagesMenuOpen) {
            return;
        }
        m_startGameButton.interactable = true;
        SFXManager.instance.PlayUIClick();
        UIManager.HideUiElement("Intro_Menu");
        LevelInfo info = new LevelInfo() { id = 0, controllerName = "G00L00END", group = 0, levelNumber = 0, name = "Endless" };
        StartLevel(info, 0);
    }

    private void MuteSFXButton()
    {
        bool mute = SFXManager.instance.IsSFXMute();
        SFXManager.instance.MuteSFX(!mute);
        SFXManager.instance.PlayUIClick();
        ChangeSFXButton();
    }

    private void MuteMusicButton()
    {
        bool mute = MusicManager.instance.IsMusicMute();
        MusicManager.instance.MuteMusic(!mute);
        SFXManager.instance.PlayUIClick();
        ChangeMusicButton();
    }

    private string GetFormatedScore(int p_score) {
        string scoreText = m_translation.score;
        return string.Format("{0}: {1}", scoreText, p_score);
    }
}
