﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cursor3DController : MonoBehaviour {

    private float m_minRow = 0;
    private float m_maxRow = 0;
    private float m_minColumn = 0;
    private float m_maxColumn = 0;
    private float m_rowOffset = 0;

    private bool m_moving = false;
    private bool m_gamePaused = false;

    private Transform cursorLeft;
    private Transform cursorRight;

    private List<float> m_reverseRow = new List<float>();

    // Use this for initialization
    void Start () {
        transform.position = GameController.Instance.BaseProperties.Cursor3DPosition;
        m_minRow = transform.position.y;
        m_maxRow = (GameController.Instance.BaseProperties.RowsCount + m_minRow) - 1;
        m_minColumn = transform.position.x;
        m_maxColumn = (GameController.Instance.BaseProperties.ColumnsCount + m_minColumn) - 2;

        int rowsCount = GameController.Instance.BaseProperties.RowsCount;
        for (int i = 0; i < rowsCount; i++)
        {
            m_reverseRow.Add(i);
        }
        m_reverseRow.Reverse();

        GameController.Instance.AddRowEvent += AddRow;
        GameController.Instance.ChangeUIPauseEvent += PauseGame;

        if (transform.childCount >= 2)
        {
            cursorLeft = transform.GetChild(0);
            cursorRight = transform.GetChild(1);
        }
        else
        {
            throw new System.Exception("Error. Cursor 3D was not setup correctly. It needs 2 child transforms");
        }
        
    }

    // Update is called once per frame
    void Update () {
        if (m_gamePaused) {
            return;
        }

        float xAxis = Input.GetAxisRaw("Horizontal");
        float yAxis = Input.GetAxisRaw("Vertical");

        if (m_moving && xAxis == 0 && yAxis == 0)
        {
            m_moving = false;
            return;
        }

        if (m_moving)
            return;

        if (yAxis != 0 || xAxis != 0)
        {
            float xPos = transform.localPosition.x + xAxis;
            float yPos = transform.localPosition.y + yAxis;

            xPos = Mathf.Clamp(xPos, m_minColumn, m_maxColumn);
            yPos = Mathf.Clamp(yPos, m_minRow + m_rowOffset, m_maxRow + m_rowOffset);

            transform.localPosition = new Vector3(xPos, yPos, transform.localPosition.z);

            m_moving = true;
        }

        if (Input.GetButtonDown("Fire1"))
        {
            Vector3 fwrd = cursorLeft.TransformDirection(Vector3.down);
            BoxMovement boxLeft = null;
            BoxMovement boxRight = null;

            RaycastHit hitInfoLeft;
            RaycastHit hitInfoRight;
            Debug.DrawRay(cursorLeft.position, fwrd, Color.red, 1, true);
            if (Physics.Raycast(cursorLeft.position, fwrd, out hitInfoLeft, 1))
            {
                boxLeft = hitInfoLeft.transform.GetComponent<BoxMovement>();
            }
            if (Physics.Raycast(cursorRight.position, fwrd, out hitInfoRight, 1))
            {
                boxRight = hitInfoRight.transform.GetComponent<BoxMovement>();
            }

            if (!ReferenceEquals(boxLeft, null))
            {
                BoxPosition bPos = boxLeft.PuzzleBox.BoxPosition;
                GameController.Instance.PuzzleBoxClicked(bPos, MoveDirection.Right);
            }
            else if(ReferenceEquals(boxLeft, null) && !ReferenceEquals(boxRight, null))
            {
                BoxPosition bPos = boxRight.PuzzleBox.BoxPosition;
                GameController.Instance.PuzzleBoxClicked(bPos, MoveDirection.Left);
            }

        }
    }

    private void AddRow()
    {
        m_rowOffset--;
        if (transform.localPosition.y >= m_maxRow + m_rowOffset)
        {
            transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y - 1, transform.localPosition.z);
        }
    }


    private void PauseGame(bool p_value) {
        m_gamePaused = p_value;
    }
}
