﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleBox {

    private GameObject m_gameObject;
    private BoxMovement m_boxMovement;
    private PuzzleBoxProperties m_properties;
    private Blocks m_blockType;
    private BoxPosition m_matrixPosition;

    public Blocks BlockType { get { return m_blockType; } }
    public BoxPosition BoxPosition { get { return m_matrixPosition; } }
    public Vector3 WorldPosition {
        get {
            return m_gameObject.transform.position;
        }
    }

    public PuzzleBox(GameObject p_go, PuzzleBoxProperties p_properties, BaseProperties p_baseProperties)
    {
        m_gameObject = p_go;
        m_properties = p_properties;

        m_boxMovement = m_gameObject.AddComponent<BoxMovement>();
        m_boxMovement.SetPuzzleBox(this, p_baseProperties.BlockMoveTime, p_baseProperties.BlockFallTime, m_properties.MeshObject);

        m_blockType = m_properties.BlockType;
    }

    public void SetPosition(int p_row, int p_column)
    {
        m_matrixPosition = new BoxPosition(p_row, p_column);
    }

    public void SetFadeColor(bool p_value)
    {
        if (p_value)
        {
            m_boxMovement.DarkenMaterial();
        }
        else
        {
            m_boxMovement.RegularMaterial();
        }
    }

    public void MoveBox(MoveDirection p_dir, BoxPosition p_newPos, Action<BoxPosition, MoveDirection> p_callBack)
    {
        m_blockType = Blocks.Animating;
        m_boxMovement.MoveBox(p_dir, p_callBack);
        m_matrixPosition = p_newPos;
    }

    public void DropBox(BoxPosition p_newPos, Action<BoxPosition, MoveDirection> p_callBack)
    {
        m_blockType = Blocks.Animating;
        int fallPositions = p_newPos.row - m_matrixPosition.row;
        m_boxMovement.DropBox(fallPositions, p_callBack);
        m_matrixPosition = p_newPos;
    }

    public void StartDestroyAnimation(Action p_callBack)
    {
        SFXManager.instance.PlayBlockSound(m_blockType);

        m_blockType = Blocks.Animating;

        m_boxMovement.StartDestroyAnimation(p_callBack);
    }

    public void StartEndGameAnimation() {
        m_blockType = Blocks.Animating;

        m_boxMovement.StartEndGameAnimation();
    }

    public void DestroyPuzzleBox()
    {
        m_gameObject.SetActive(false);
    }

    public void RevertBlockType()
    {
        m_blockType = m_properties.BlockType;
    }
}
