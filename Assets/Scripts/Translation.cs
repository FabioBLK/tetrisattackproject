﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Translation {

    private TranslationStrings m_currentStrings;

    public TranslationStrings CurrentTranslation { get { return m_currentStrings; } }

    public Translation(SystemLanguage p_userLanguage) {
        TextAsset textFile = Resources.Load<TextAsset>("translations");
        //Debug.LogWarning(textFile.text);

        if (!ReferenceEquals(textFile, null)) {
            JSONTranslation translation = JsonUtility.FromJson<JSONTranslation>(textFile.text);

            if(p_userLanguage == SystemLanguage.English) {
                m_currentStrings = translation.english;
            }
            else if(p_userLanguage == SystemLanguage.Portuguese) {
                m_currentStrings = translation.portuguese;
            }
            else if (p_userLanguage == SystemLanguage.Spanish) {
                m_currentStrings = translation.spanish;
            }
            else if (p_userLanguage == SystemLanguage.French) {
                m_currentStrings = translation.french;
            }
            else if (p_userLanguage == SystemLanguage.German) {
                m_currentStrings = translation.german;
            }
            else if (p_userLanguage == SystemLanguage.Italian) {
                m_currentStrings = translation.italian;
            }
            else {
                m_currentStrings = translation.english;
            }
        }
    }
}
