﻿using System.Collections.Generic;

[System.Serializable]
public class JSONLevelGroup {
    public List<LevelInfo> levelGroup;
}

[System.Serializable]
public class LevelInfo {
    public int id;
    public string name;
    public int group;
    public int levelNumber;
    public string controllerName;
}
