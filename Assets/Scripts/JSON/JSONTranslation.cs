﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class JSONTranslation {

    public TranslationStrings english;
    public TranslationStrings portuguese;
    public TranslationStrings spanish;
    public TranslationStrings french;
    public TranslationStrings german;
    public TranslationStrings italian;
}

[System.Serializable]
public class TranslationStrings {

    public string game_name;
    public string endless_mode;
    public string combo_mode;
    public string level_mode;
    public string row_mode;
    public string endless_info;
    public string combo_info_begin;
    public string combo_info_end;
    public string level_info;
    public string row_info_begin;
    public string row_info_end;
    public string continue_btn;
    public string restart_btn;
    public string quit_btn;
    public string game_over;
    public string score;
    public string level;
    public string moves;
    public string stage;
    public string stages;
    public string combos;
    public string congratulations;
    public string stage_clear;
    public string endless_type;
    public string combo_type;
    public string row_type;
    public string level_type;
    public string select_mode;
    public string stages_mode;
    public string moves_left_begin;
    public string moves_left_end;
}