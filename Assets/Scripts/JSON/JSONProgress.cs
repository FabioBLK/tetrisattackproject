﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class JSONProgress {
    public List<LevelProgress> levels;

    public JSONProgress() {
        levels = new List<LevelProgress>();
    }
}

[System.Serializable]
public class LevelProgress {
    public int levelId;
    public bool complete;
    public int stars;

    public LevelProgress(int p_id, bool p_complete) {
        levelId = p_id;
        complete = p_complete;
        stars = 0;
    }
}
