﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RowSpeed
{
    private float m_minRowSpeed;
    private float m_maxRowSpeed;
    private float m_currentSpeed;
    private float m_scoreThreshold;
    private float m_maxScoreThreshold;

    public float CurrentSpeed { get { return m_currentSpeed; } }

    public RowSpeed(float p_minRowSpeed, float p_maxRowSpeed, float p_scoreThreshold, float p_maxScoreThreshold, float m_maxSpeedBoost, float m_minSpeedBoost)
    {
        m_minRowSpeed = p_minRowSpeed;
        m_maxRowSpeed = p_maxRowSpeed;
        m_scoreThreshold = p_scoreThreshold;
        m_maxScoreThreshold = p_maxScoreThreshold;

        m_currentSpeed = m_minRowSpeed;
    }

    public float GetNewSpeed(int p_currentScore)
    {
        float percentage = (p_currentScore / m_maxScoreThreshold) * m_scoreThreshold;

        float inversePercentage = 100 - percentage;

        int floorPercentage = (int)inversePercentage;

        float newSpeed = floorPercentage * ((m_maxRowSpeed - m_minRowSpeed) / 100);
        newSpeed = newSpeed + m_minRowSpeed;
        m_currentSpeed = newSpeed;

        return Mathf.Clamp(m_currentSpeed, m_minRowSpeed, m_maxRowSpeed);
    }
}
