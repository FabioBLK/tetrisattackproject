﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;

public class AdsRequester {

    private BannerView m_bannerView;
    private InterstitialAd m_interstitial;

    private Action m_closeCallBack;

    public AdsRequester() {
#if UNITY_ANDROID
        string appId = "ca-app-pub-1060172145249514~7151717005";
#elif UNITY_IPHONE
            string appId = "ca-app-pub-1060172145249514~7093266438";
#else
            string appId = "unexpected_platform";
#endif

        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(appId);
    }

    public void RequestBanner() {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-1060172145249514/4485600183";
        if (Debug.isDebugBuild) {
            adUnitId = "ca-app-pub-3940256099942544/6300978111";
        }
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-1060172145249514/3428094410";
        if (Debug.isDebugBuild) {
            adUnitId = "ca-app-pub-3940256099942544/2934735716";
        }
#else
        string adUnitId = "unexpected_platform";
#endif

        // Create a 320x50 banner at the top of the screen.
        m_bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);

        AdRequest request = new AdRequest.Builder().Build();

        m_bannerView.LoadAd(request);
        m_bannerView.OnAdLoaded += OnBannerLoaded;
        m_bannerView.OnAdFailedToLoad += OnBannerFailedToLoad;
    }

    public void ShowBanner() {
        m_bannerView.Show();
    }

    public void HideBanner() {
        m_bannerView.Hide();
    }

    public void RequestInterstitial() {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-1060172145249514/8233273504";
        if (Debug.isDebugBuild) {
            adUnitId = "ca-app-pub-3940256099942544/1033173712";
        }
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-1060172145249514/5862686067";
        if (Debug.isDebugBuild) {
            adUnitId = "ca-app-pub-3940256099942544/4411468910";
        }
#else
        string adUnitId = "unexpected_platform";
#endif

        // Initialize an InterstitialAd.
        //Debug.LogWarning("request interstitial " + adUnitId);
        m_interstitial = new InterstitialAd(adUnitId);

        AdRequest request = new AdRequest.Builder().Build();
        
        m_interstitial.LoadAd(request);
        m_interstitial.OnAdLoaded += OnInterstitialLoaded;
        m_interstitial.OnAdFailedToLoad += OnInterstitialFailedToLoad;
        m_interstitial.OnAdClosed += OnInterstitialClosed;
    }

    private void OnInterstitialLoaded(object sender, EventArgs e) {
        Debug.LogWarning("interstitial loaded");
    }

    public void ShowInterstitial(Action p_callBack) {
        m_closeCallBack = p_callBack;
        Debug.LogWarning("Show interstitial");
        if (m_interstitial.IsLoaded()) {
            m_interstitial.Show();
        }
        else {
            m_closeCallBack();
            Debug.LogWarning("interstitial show failed");
        }
#if UNITY_EDITOR
        m_closeCallBack();
#endif
    }

    private void OnInterstitialClosed(object sender, EventArgs e) {
        m_closeCallBack();
        m_interstitial.Destroy();
    }

    private void OnInterstitialFailedToLoad(object sender, AdFailedToLoadEventArgs e) {
        Debug.LogWarning(string.Format("Load interstitial failed with error {0}", e.Message));
    }

    private void OnBannerFailedToLoad(object sender, AdFailedToLoadEventArgs e) {
        Debug.LogWarning(string.Format("Load banner failed with error {0}", e.Message));
    }

    private void OnBannerLoaded(object sender, System.EventArgs e) {
        m_bannerView.Hide();
    }
}
