﻿public delegate void SetPause(bool p_value);

public delegate void SetUIPause(bool p_value);

public delegate void SetSpeed(float p_value);

public delegate void SetInterval(float p_value);

public delegate void RowAdded();

public delegate void RowGameIndexNull();

public delegate void UpdateUIData(Score p_score, int p_combosLeft);

public delegate void SetStartedGame(LevelInfo p_level);

public delegate void SetEndGame(string p_reason);

public delegate void SetCompleteGame(StarsCount p_stars);

public delegate void MoveOccured(int m_movesLeft);