﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score {

    private int m_score;
    private int m_point;
    private int m_multiplierCount;
    private int m_multiplierScore;
    private int m_comboScore;
    private float m_scoreThreshold;
    private float m_maxScoreThreshold;
    private List<float> m_scoreBoost = new List<float>();
    private int m_scoreListSize;

    public int CurrentScore { get { return m_score; } }

    public Score(int p_pointValue, int p_multiplierCount, int p_multiplierScore, int p_comboScore, int p_initialScore, float p_scoreThreshold, float p_maxScoreThreshold, int p_scoreMinBoost, int p_scoreMaxBoost)
    {
        m_score = p_initialScore;
        m_point = p_pointValue;
        m_multiplierCount = p_multiplierCount;
        m_multiplierScore = p_multiplierScore;
        m_comboScore = p_comboScore;
        m_scoreThreshold = p_scoreThreshold;
        m_maxScoreThreshold = p_maxScoreThreshold;
        m_scoreBoost = ExtraMath.CurvedThreshold((int)(m_maxScoreThreshold / m_scoreThreshold), p_scoreMaxBoost, p_scoreMinBoost);
        m_scoreListSize = m_scoreBoost.Count - 1;
    }

    public int AddSinglePoint()
    {
        m_score += m_point;
        m_score += (int)m_scoreBoost[Mathf.Clamp(GetCurrentLevel(), 0, m_scoreListSize)];
        //Debug.Log("Boost = " + (int)m_scoreBoost[GetCurrentLevel()]);
        return m_score;
    }

    public int AddMultiplePoints(int p_amountOfPoints, bool p_combo = false)
    {
        m_score += (m_point * p_amountOfPoints);
        if(p_amountOfPoints > m_multiplierCount)
        {
            int count = p_amountOfPoints - m_multiplierCount;
            int multiplier = count * m_multiplierScore;
            m_score += multiplier;
        }

        if (p_combo)
        {
            m_score += m_comboScore;
        }

        m_score += (int)m_scoreBoost[Mathf.Clamp(GetCurrentLevel(), 0, m_scoreListSize)];
        //Debug.Log("Boost = " + (int)m_scoreBoost[GetCurrentLevel()]);
        return m_score;
    }

    public int AddRawScore(int p_score)
    {
        m_score += p_score;
        m_score += (int)m_scoreBoost[Mathf.Clamp(GetCurrentLevel(), 0, m_scoreListSize)];
        //Debug.Log("Boost = " + (int)m_scoreBoost[GetCurrentLevel()]);
        return m_score;
    }

    public int GetCurrentLevel() {
        return m_score / TotalOfLevels();
    }

    public int TotalOfLevels() {
        return (int)(m_maxScoreThreshold / m_scoreThreshold);
    }

    public float GetDecimalToNextLevel() {
        float percentage = (m_score / m_maxScoreThreshold) * m_scoreThreshold;
        return percentage % 1;
    }
}
