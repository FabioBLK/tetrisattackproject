﻿using System.Collections.Generic;

public static class Directions {

    public static MoveDirection OpositeDirection(this MoveDirection p_dir) {
        MoveDirection result = OpositeDictionary[p_dir];
        return result;
    }

    public static int DirectionValue(this MoveDirection p_dir) {
        int result = DirectionValueDictionary[p_dir];
        return result;
    }

    public static int OpositeDirectionValue(this MoveDirection p_dir) {
        MoveDirection oposite = OpositeDictionary[p_dir];
        int result = DirectionValueDictionary[oposite];
        return result;
    }

    private static Dictionary<MoveDirection, MoveDirection> OpositeDictionary = new Dictionary<MoveDirection, MoveDirection>
    {
        {MoveDirection.Up, MoveDirection.Down },
        {MoveDirection.Down, MoveDirection.Up },
        {MoveDirection.Left, MoveDirection.Right },
        {MoveDirection.Right, MoveDirection.Left }
    };

    private static Dictionary<MoveDirection, int> DirectionValueDictionary = new Dictionary<MoveDirection, int>
    {
        {MoveDirection.Up, -1 },
        {MoveDirection.Right, 1 },
        {MoveDirection.Down, 1 },
        {MoveDirection.Left, -1 }
    };
}
