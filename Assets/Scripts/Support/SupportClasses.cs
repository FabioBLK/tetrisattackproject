﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameModeType
{
    Endless,
    Row,
    Combo,
    Puzzle,
    Level
}

public enum Blocks
{
    Diamond,
    Fire,
    Gold,
    Heart,
    Leaf,
    Water,
    Animating
}

public enum MoveDirection
{
    Up,
    Down,
    Left,
    Right
}

public enum StarsCount 
{
    One,
    Two,
    Three
}
