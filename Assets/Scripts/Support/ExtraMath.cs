﻿using System.Collections.Generic;
using UnityEngine;

public static class ExtraMath {

    private static Vector3 v0 = new Vector3(50, 0, 0);
    private static Vector3 v1 = new Vector3(20, 0, 0);
    private static Vector3 v2 = new Vector3(10, 0, 0);
    private static Vector3 v3 = new Vector3(00, 0, 0);

    public static Vector3 CubeBezier3(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t) {
        return (((-p0 + 3 * (p1 - p2) + p3) * t + (3 * (p0 + p2) - 6 * p1)) * t + 3 * (p1 - p0)) * t + p0;
    }

    public static Vector3 CubeBezier3(float t) {
        return (((-v1 + 3 * (v1 - v2) + v3) * t + (3 * (v0 + v2) - 6 * v1)) * t + 3 * (v1 - v0)) * t + v0;
    }

    public static List<float> CurvedThreshold(int p_levels, int p_maxBoost, int p_minBoost) {
        //TODO - IN DEVELOPMENT
        List<float> result = new List<float>();

        for (int i = 0; i < p_levels; i++) {
            float t = (float)i / p_levels;
            t = Mathf.Sin(t * Mathf.PI * 0.5f);
            float lerp = Mathf.Lerp(p_maxBoost, p_minBoost, t);
            //Debug.LogWarning(lerp);
            //Vector3 res = CubeBezier3(v0, v1, v2, v3, t);
            //result.Add(res.x);
            //Debug.LogWarning(res.x);

            float lerpClamped = Mathf.Clamp(lerp, p_minBoost, p_maxBoost);
            //Debug.LogWarning(lerpClamped);
            result.Add(lerpClamped);
        }

        return result;
    }

    public static float CurvedLerp(int p_percentage, int p_levels, float p_maxBoost, float p_minBoost) {
        float t = (float)p_percentage / p_levels;
        t = Mathf.Sin(t * Mathf.PI * 0.5f);
        float lerp = Mathf.Lerp(p_maxBoost, p_minBoost, t);

        Debug.LogWarning("lerp is : " + lerp);
        return lerp;
    }
}