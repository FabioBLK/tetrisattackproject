﻿using System.Collections.Generic;

public static class UINames {

    private static readonly List<string> m_baseElements = new List<string> {
        { "BackgroundElement" },
        { "ButtonsElement" },
        { "Items" }
    };

    private static readonly Dictionary<GameModeType, string> m_gameModeShort = new Dictionary<GameModeType, string> {
        { GameModeType.Combo, "COM_" },
        { GameModeType.Endless, "END_"},
        { GameModeType.Puzzle, "PUZ_" },
        { GameModeType.Row, "ROW_" },
        { GameModeType.Level, "LVL_" }
    };

    private static readonly Dictionary<GameModeType, string> m_gameModeFull = new Dictionary<GameModeType, string> {
        { GameModeType.Combo, "Combo" },
        { GameModeType.Endless, "Endless"},
        { GameModeType.Puzzle, "Puzzle" },
        { GameModeType.Row, "Row" },
        { GameModeType.Level, "Level" },
    };

    public static List<string> GameElementsList(GameModeType p_type) {
        List<string> result = new List<string>();
        for (int i = 0; i < m_baseElements.Count; i++) {
            result.Add(string.Concat(m_gameModeShort[p_type], m_baseElements[i]));
        }
        return result;
    }

    public static string GetShortNameMode(GameModeType p_type) {
        return m_gameModeShort[p_type];
    }
}
