﻿public class BoxPosition {
    public int row;
    public int column;

    public BoxPosition() {
        row = 0;
        column = 0;
    }

    public BoxPosition(int p_row, int p_column) {
        row = p_row;
        column = p_column;
    }
}
